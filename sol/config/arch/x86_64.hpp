#if !defined(sol_config_arch_x86_64_hpp_included)
#define sol_config_arch_x86_64_hpp_included
/// \addtogroup sol_config
/// \{


/**
 * \file sol/config/arch/x86_64.hpp
 * \brief x86_64-specific configuration
 * \author Sven Suursoho
 */


#define SOL_ARCH_X86_64 1

#define SOL_ARCH_NAME "x86_64"


#if !defined(SOL_BYTE_ORDER)
    /// Byte ordering
    #define SOL_BYTE_ORDER SOL_LITTLE_ENDIAN
#endif


/// \}
#endif  // sol_config_arch_x86_64_hpp_included
