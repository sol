#if !defined(sol_cstdint_hpp_included)
#define sol_cstdint_hpp_included
/// \addtogroup sol
/// \{


/**
 * \file sol/cstdint.hpp
 * \brief Include system cstdint or provide corresponding types
 * \author Sven Suursoho
 */


#include <sol/config.hpp>

#if defined(SOL_HAS_STDINT_H)
    #include <stdint.h>
#endif


/// \}
#endif  // sol_cstdint_hpp_included
