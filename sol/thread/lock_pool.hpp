#if !defined(sol_thread_lock_pool_hpp_included)
#define sol_thread_lock_pool_hpp_included
/// \addtogroup sol_thread
/// \{


/**
 * \file sol/thread/lock_pool.hpp
 * \brief Pool of lockable objects
 * \author Sven Suursoho
 */


#include <cassert>
#include <vector>


namespace sol { namespace thread {


/**
 * Pool of Lockable objects.
 *
 * Convenience class that provides fixed number of Lockable objets. Typical
 * use of lock_pool is in situation where it is not possible or optimal to use
 * one Lockable per protected object. lock_pool will return Lockable object
 * based on address of the object that needs to be protected.
 */
template <typename Lockable>
class lock_pool
{
public:

    /// Lockable type
    typedef Lockable lock_type;


    /// Create pool of \a size Lockable objects
    lock_pool (size_t size)
        : size_(size)
        , pool_(size)
    {
        assert(size_ > 0);
    }


    /// Return reference to Lockable found by hash of \a address
    lock_type &
    operator() (void const *address) const
    {
        size_t const hash(size_t(address) >> (sizeof(address) >> 1));
        return pool[hash%size_];
    }


private:

    size_t const size_;
    std::vector<lock_type> pool_;
};


}}  // namespace sol::thread


/// \}
#endif  // sol_thread_lock_pool_hpp_included
