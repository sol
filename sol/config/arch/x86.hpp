#if !defined(sol_config_arch_x86_hpp_included)
#define sol_config_arch_x86_hpp_included
/// \addtogroup sol_config
/// \{


/**
 * \file sol/config/arch/x86.hpp
 * \brief x86-specific configuration
 * \author Sven Suursoho
 */


#define SOL_ARCH_X86 1

#define SOL_ARCH_NAME "x86"


#if !defined(SOL_BYTE_ORDER)
    /// Byte ordering
    #define SOL_BYTE_ORDER SOL_LITTLE_ENDIAN
#endif


/// \}
#endif  // sol_config_arch_x86_hpp_included
