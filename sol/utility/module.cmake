sol_headers(
    ../utility.hpp
    common_type.hpp
    conditional.hpp
    integral_constant.hpp
)


sol_tests(
    common_type_test.cpp
    conditional_test.cpp
    integral_constant_test.cpp
)
