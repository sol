sol_headers(
    ../atomic.hpp
    backoff.hpp
    flag.hpp
    operations.hpp
)


sol_tests(
    flag_test.cpp
    operations_test.cpp
)
