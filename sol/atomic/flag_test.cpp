#include <sol/test_main.cpp>
#include <sol/atomic/backoff.hpp>
#include <sol/atomic/flag.hpp>


namespace sol { namespace test {


namespace atomic = sol::atomic;


int
run (std::vector<std::string> const &args)
{
    {
        sol_info("flag");
        atomic::flag f;

        sol_equal(f.test(), false);
        sol_equal(f.test_and_set(), false);
        sol_equal(f.test(), true);
        sol_equal(f.test_and_set(), true);
        sol_equal(f.test_and_set(), true);
        f.clear();
        sol_equal(f.test(), false);
        sol_equal(f.test_and_set(), false);
        sol_equal(f.test(), true);
        sol_equal(f.test_and_set(), true);
        sol_equal(f.test_and_set(), true);
    }


    {
        sol_info("spinlock");
        atomic::flag f;

        int const N(2);

        sol_equal(f.test(), false);
        sol_equal(f.set(atomic::spin_count(N)), true);
        sol_equal(f.test(), true);
        sol_equal(f.set(atomic::spin_count(N)), false);
        sol_equal(f.try_set(), false);
        sol_equal(f.test(), true);
        f.clear();

        sol_equal(f.test(), false);
        sol_equal(f.try_set(), true);
        sol_equal(f.test(), true);
        sol_equal(f.try_set(), false);
        sol_equal(f.set(atomic::spin_count(N)), false);
        f.clear();
        sol_equal(f.test(), false);
    }


    return EXIT_SUCCESS;
}


}}  // namespace sol::test
