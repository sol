#if !defined(sol_math_sign_hpp_included)
#define sol_math_sign_hpp_included
/// \addtogroup sol_math
/// \{


/**
 * \file sol/math/sign.hpp
 * \brief Compile-time sign taking
 * \author Sven Suursoho
 */


#include <sol/cstdint.hpp>
#include <sol/utility/integral_constant.hpp>


namespace sol { namespace math {


/// Take sign of V during compile-time
template <intmax_t V>
struct sign
    : integral_constant<intmax_t, (V < 0) ? -1 : 1>
{};


}}  // namespace sol::math


/// \}
#endif  // sol_math_sign_hpp_included
