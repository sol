#if !defined(sol_atomic_backoff_hpp_included)
#define sol_atomic_backoff_hpp_included
/// \addtogroup sol_atomic
/// \{


/**
 * \file sol/atomic/backoff.hpp
 * \brief Backoff algorithms for spinning atomic operations.
 * \author Sven Suursoho
 */


#include <sol/date_time/hiresolution_clock.hpp>


namespace sol { namespace atomic {


/**
 * Do not spin, return immediately
 *
 * This algorithm is actually quite pointless, it's effect is as doing atomic
 * operation once. It is implemented only for API completness
 */
struct no_spin
{
    /// Return always true (backoff after one try)
    bool
    operator() (unsigned long) const
    {
        return true;
    }
};


/**
 * Spin until success
 *
 * It is not recommended to use this backoff algorithm as it does scale very
 * badly because operation is tried in tight loop without possiblity to give
 * up remaining timeslice on failure.
 */
struct spin
{
    /// Return always false (spin until operation succeeds)
    bool
    operator() (unsigned long) const
    {
        return false;
    }
};


/**
 * Stop spinning after specified count iterations.
 *
 * Use only with small values assuming that atomic operation caller can handle
 * atomic operation failure (i.e. do some other work)
 */
struct spin_count
{
    /// Number of iteration
    unsigned long const count;


    /// Construct backoff for \a count iterations
    spin_count (unsigned long count)
        : count(count)
    {}


    /// Backoff if \a current_iteration has reached \e count
    bool
    operator() (unsigned long current_iteration) const
    {
        return current_iteration == count;
    }
};


/**
 * Suspend caller during loop in retry to do atomic operation.
 *
 * It is almost always best to use this as it scales quite well and if atomic
 * operation does not succeed immediately, it consumes no little OS resources
 */
struct spin_sleep
{
    /// Sleep with time growing exponentially with \a current_iteration (but
    /// no more than 65.535ms during one iteration)
    bool
    operator() (unsigned long current_iteration) const;
};


/**
 * Suspend caller during loop in retry to do atomic operation.
 *
 * Mostly has same properties as spin_sleep except it has possibility to
 * provide upper time limit for retrying
 */
struct spin_until
{
    /// Backing off time
    date_time::utc_time const &expire_time;


    /// Construct backoff functor that expires at specified \a time
    spin_until (date_time::utc_time const &time)
        : expire_time(time)
    {}


    /// \see spin_sleep::operator()
    bool
    operator() (unsigned long current_iteration) const
    {
        if (date_time::hiresolution_clock::universal_time() < expire_time) {
            static spin_sleep sleep_backoff;
            return sleep_backoff(current_iteration);
        }

        // expired
        return true;
    }
};


}}  // namespace sol::atomic


#include <sol/atomic/bits/backoff.hpp>


/// \}
#endif  // sol_atomic_backoff_hpp_included
