sol_headers(
    ../date_time.hpp
    duration.hpp
    hiresolution_clock.hpp
    utc_time.hpp
)


sol_tests(
    duration_test.cpp
    utc_time_test.cpp
)
