#if !defined(sol_utility_common_type_hpp_included)
#define sol_utility_common_type_hpp_included
/// \addtogroup sol
/// \{


/**
 * \file sol/utility/common_type.hpp
 * \brief From multiple types, find one that all can be converted to
 * \author Sven Suursoho
 *
 * \see http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2661.htm#common_type
 */


#include <sol/config.hpp>
#include <sol/utility/integral_constant.hpp>


namespace sol {


#if defined(SOL_HAS_DECLTYPE) && defined(SOL_HAS_VARIADIC_TEMPLATE)


//
// Use fancy C++0x features
//


/// For template parameter types, find common type to which all can be converted
template <typename ...T>
struct common_type;


/// \internal
template <typename T>
struct common_type<T>
{
    typedef T type;
};


/// \internal
template <typename T1, typename T2>
struct common_type<T1, T2>
{
private:
    static T1 &&t1 ();
    static T2 &&t2 ();

public:
    typedef decltype(bool() ? t1() : t2()) type;
};


/// \internal
template <typename T1, typename T2, typename ...T>
struct common_type<T1, T2, T...>
{
    typedef typename common_type<
        typename common_type<T1, T2>::type, T...
    >::type type;
};


#else   // defined(SOL_HAS_DECLTYPE) && defined(SOL_HAS_VARIADIC_TEMPLATE)


template <typename Type1, typename Type2>
struct common_type_;

#define SOL_COMMON_TYPE(T1,T2,RT) \
    template <> struct common_type_<T1, T2> { typedef RT type; }

SOL_COMMON_TYPE(bool, bool, bool);
SOL_COMMON_TYPE(bool, char, int);
SOL_COMMON_TYPE(bool, unsigned char, int);
SOL_COMMON_TYPE(bool, short, int);
SOL_COMMON_TYPE(bool, unsigned short, int);
SOL_COMMON_TYPE(bool, int, int);
SOL_COMMON_TYPE(bool, unsigned int, unsigned int);
SOL_COMMON_TYPE(bool, long, long);
SOL_COMMON_TYPE(bool, unsigned long, unsigned long);
SOL_COMMON_TYPE(bool, long long, long long);
SOL_COMMON_TYPE(bool, unsigned long long, unsigned long long);
SOL_COMMON_TYPE(bool, float, float);
SOL_COMMON_TYPE(bool, double, double);
SOL_COMMON_TYPE(bool, long double, long double);

SOL_COMMON_TYPE(char, bool, int);
SOL_COMMON_TYPE(char, char, char);
SOL_COMMON_TYPE(char, unsigned char, int);
SOL_COMMON_TYPE(char, short, int);
SOL_COMMON_TYPE(char, unsigned short, int);
SOL_COMMON_TYPE(char, int, int);
SOL_COMMON_TYPE(char, unsigned int, unsigned int);
SOL_COMMON_TYPE(char, long, long);
SOL_COMMON_TYPE(char, unsigned long, unsigned long);
SOL_COMMON_TYPE(char, long long, long long);
SOL_COMMON_TYPE(char, unsigned long long, unsigned long long);
SOL_COMMON_TYPE(char, float, float);
SOL_COMMON_TYPE(char, double, double);
SOL_COMMON_TYPE(char, long double, long double);

SOL_COMMON_TYPE(unsigned char, bool, int);
SOL_COMMON_TYPE(unsigned char, char, int);
SOL_COMMON_TYPE(unsigned char, unsigned char, unsigned char);
SOL_COMMON_TYPE(unsigned char, short, int);
SOL_COMMON_TYPE(unsigned char, unsigned short, int);
SOL_COMMON_TYPE(unsigned char, int, int);
SOL_COMMON_TYPE(unsigned char, unsigned int, unsigned int);
SOL_COMMON_TYPE(unsigned char, long, long);
SOL_COMMON_TYPE(unsigned char, unsigned long, unsigned long);
SOL_COMMON_TYPE(unsigned char, long long, long long);
SOL_COMMON_TYPE(unsigned char, unsigned long long, unsigned long long);
SOL_COMMON_TYPE(unsigned char, float, float);
SOL_COMMON_TYPE(unsigned char, double, double);
SOL_COMMON_TYPE(unsigned char, long double, long double);

SOL_COMMON_TYPE(short, bool, int);
SOL_COMMON_TYPE(short, char, int);
SOL_COMMON_TYPE(short, unsigned char, int);
SOL_COMMON_TYPE(short, short, short);
SOL_COMMON_TYPE(short, unsigned short, int);
SOL_COMMON_TYPE(short, int, int);
SOL_COMMON_TYPE(short, unsigned int, unsigned int);
SOL_COMMON_TYPE(short, long, long);
SOL_COMMON_TYPE(short, unsigned long, unsigned long);
SOL_COMMON_TYPE(short, long long, long long);
SOL_COMMON_TYPE(short, unsigned long long, unsigned long long);
SOL_COMMON_TYPE(short, float, float);
SOL_COMMON_TYPE(short, double, double);
SOL_COMMON_TYPE(short, long double, long double);

SOL_COMMON_TYPE(unsigned short, bool, int);
SOL_COMMON_TYPE(unsigned short, char, int);
SOL_COMMON_TYPE(unsigned short, unsigned char, int);
SOL_COMMON_TYPE(unsigned short, short, int);
SOL_COMMON_TYPE(unsigned short, unsigned short, unsigned short);
SOL_COMMON_TYPE(unsigned short, int, int);
SOL_COMMON_TYPE(unsigned short, unsigned int, unsigned int);
SOL_COMMON_TYPE(unsigned short, long, long);
SOL_COMMON_TYPE(unsigned short, unsigned long, unsigned long);
SOL_COMMON_TYPE(unsigned short, long long, long long);
SOL_COMMON_TYPE(unsigned short, unsigned long long, unsigned long long);
SOL_COMMON_TYPE(unsigned short, float, float);
SOL_COMMON_TYPE(unsigned short, double, double);
SOL_COMMON_TYPE(unsigned short, long double, long double);

SOL_COMMON_TYPE(int, bool, int);
SOL_COMMON_TYPE(int, char, int);
SOL_COMMON_TYPE(int, unsigned char, int);
SOL_COMMON_TYPE(int, short, int);
SOL_COMMON_TYPE(int, unsigned short, int);
SOL_COMMON_TYPE(int, int, int);
SOL_COMMON_TYPE(int, unsigned int, unsigned int);
SOL_COMMON_TYPE(int, long, long);
SOL_COMMON_TYPE(int, unsigned long, unsigned long);
SOL_COMMON_TYPE(int, long long, long long);
SOL_COMMON_TYPE(int, unsigned long long, unsigned long long);
SOL_COMMON_TYPE(int, float, float);
SOL_COMMON_TYPE(int, double, double);
SOL_COMMON_TYPE(int, long double, long double);

SOL_COMMON_TYPE(unsigned int, bool, unsigned int);
SOL_COMMON_TYPE(unsigned int, char, unsigned int);
SOL_COMMON_TYPE(unsigned int, unsigned char, unsigned int);
SOL_COMMON_TYPE(unsigned int, short, unsigned int);
SOL_COMMON_TYPE(unsigned int, unsigned short, unsigned int);
SOL_COMMON_TYPE(unsigned int, int, unsigned int);
SOL_COMMON_TYPE(unsigned int, unsigned int, unsigned int);
SOL_COMMON_TYPE(unsigned int, long, long);
SOL_COMMON_TYPE(unsigned int, unsigned long, unsigned long);
SOL_COMMON_TYPE(unsigned int, long long, long long);
SOL_COMMON_TYPE(unsigned int, unsigned long long, unsigned long long);
SOL_COMMON_TYPE(unsigned int, float, float);
SOL_COMMON_TYPE(unsigned int, double, double);
SOL_COMMON_TYPE(unsigned int, long double, long double);

SOL_COMMON_TYPE(long, bool, long);
SOL_COMMON_TYPE(long, char, long);
SOL_COMMON_TYPE(long, unsigned char, long);
SOL_COMMON_TYPE(long, short, long);
SOL_COMMON_TYPE(long, unsigned short, long);
SOL_COMMON_TYPE(long, int, long);
SOL_COMMON_TYPE(long, unsigned int, long);
SOL_COMMON_TYPE(long, long, long);
SOL_COMMON_TYPE(long, unsigned long, unsigned long);
SOL_COMMON_TYPE(long, long long, long long);
SOL_COMMON_TYPE(long, unsigned long long, unsigned long long);
SOL_COMMON_TYPE(long, float, float);
SOL_COMMON_TYPE(long, double, double);
SOL_COMMON_TYPE(long, long double, long double);

SOL_COMMON_TYPE(unsigned long, bool, unsigned long);
SOL_COMMON_TYPE(unsigned long, char, unsigned long);
SOL_COMMON_TYPE(unsigned long, unsigned char, unsigned long);
SOL_COMMON_TYPE(unsigned long, short, unsigned long);
SOL_COMMON_TYPE(unsigned long, unsigned short, unsigned long);
SOL_COMMON_TYPE(unsigned long, int, unsigned long);
SOL_COMMON_TYPE(unsigned long, unsigned int, unsigned long);
SOL_COMMON_TYPE(unsigned long, long, unsigned long);
SOL_COMMON_TYPE(unsigned long, unsigned long, unsigned long);
SOL_COMMON_TYPE(unsigned long, long long, unsigned long long);
SOL_COMMON_TYPE(unsigned long, unsigned long long, unsigned long long);
SOL_COMMON_TYPE(unsigned long, float, float);
SOL_COMMON_TYPE(unsigned long, double, double);
SOL_COMMON_TYPE(unsigned long, long double, long double);

SOL_COMMON_TYPE(long long, bool, long long);
SOL_COMMON_TYPE(long long, char, long long);
SOL_COMMON_TYPE(long long, unsigned char, long long);
SOL_COMMON_TYPE(long long, short, long long);
SOL_COMMON_TYPE(long long, unsigned short, long long);
SOL_COMMON_TYPE(long long, int, long long);
SOL_COMMON_TYPE(long long, unsigned int, long long);
SOL_COMMON_TYPE(long long, long, long long);
SOL_COMMON_TYPE(long long, unsigned long, unsigned long long);
SOL_COMMON_TYPE(long long, long long, long long);
SOL_COMMON_TYPE(long long, unsigned long long, unsigned long long);
SOL_COMMON_TYPE(long long, float, float);
SOL_COMMON_TYPE(long long, double, double);
SOL_COMMON_TYPE(long long, long double, long double);

SOL_COMMON_TYPE(unsigned long long, bool, unsigned long long);
SOL_COMMON_TYPE(unsigned long long, char, unsigned long long);
SOL_COMMON_TYPE(unsigned long long, unsigned char, unsigned long long);
SOL_COMMON_TYPE(unsigned long long, short, unsigned long long);
SOL_COMMON_TYPE(unsigned long long, unsigned short, unsigned long long);
SOL_COMMON_TYPE(unsigned long long, int, unsigned long long);
SOL_COMMON_TYPE(unsigned long long, unsigned int, unsigned long long);
SOL_COMMON_TYPE(unsigned long long, long, unsigned long long);
SOL_COMMON_TYPE(unsigned long long, unsigned long, unsigned long long);
SOL_COMMON_TYPE(unsigned long long, long long, unsigned long long);
SOL_COMMON_TYPE(unsigned long long, unsigned long long, unsigned long long);
SOL_COMMON_TYPE(unsigned long long, float, float);
SOL_COMMON_TYPE(unsigned long long, double, double);
SOL_COMMON_TYPE(unsigned long long, long double, long double);

SOL_COMMON_TYPE(float, bool, float);
SOL_COMMON_TYPE(float, char, float);
SOL_COMMON_TYPE(float, unsigned char, float);
SOL_COMMON_TYPE(float, short, float);
SOL_COMMON_TYPE(float, unsigned short, float);
SOL_COMMON_TYPE(float, int, float);
SOL_COMMON_TYPE(float, unsigned int, float);
SOL_COMMON_TYPE(float, long, float);
SOL_COMMON_TYPE(float, unsigned long, float);
SOL_COMMON_TYPE(float, long long, float);
SOL_COMMON_TYPE(float, unsigned long long, float);
SOL_COMMON_TYPE(float, float, float);
SOL_COMMON_TYPE(float, double, double);
SOL_COMMON_TYPE(float, long double, long double);

SOL_COMMON_TYPE(double, bool, double);
SOL_COMMON_TYPE(double, char, double);
SOL_COMMON_TYPE(double, unsigned char, double);
SOL_COMMON_TYPE(double, short, double);
SOL_COMMON_TYPE(double, unsigned short, double);
SOL_COMMON_TYPE(double, int, double);
SOL_COMMON_TYPE(double, unsigned int, double);
SOL_COMMON_TYPE(double, long, double);
SOL_COMMON_TYPE(double, unsigned long, double);
SOL_COMMON_TYPE(double, long long, double);
SOL_COMMON_TYPE(double, unsigned long long, double);
SOL_COMMON_TYPE(double, float, double);
SOL_COMMON_TYPE(double, double, double);
SOL_COMMON_TYPE(double, long double, long double);

SOL_COMMON_TYPE(long double, bool, long double);
SOL_COMMON_TYPE(long double, char, long double);
SOL_COMMON_TYPE(long double, unsigned char, long double);
SOL_COMMON_TYPE(long double, short, long double);
SOL_COMMON_TYPE(long double, unsigned short, long double);
SOL_COMMON_TYPE(long double, int, long double);
SOL_COMMON_TYPE(long double, unsigned int, long double);
SOL_COMMON_TYPE(long double, long, long double);
SOL_COMMON_TYPE(long double, unsigned long, long double);
SOL_COMMON_TYPE(long double, long long, long double);
SOL_COMMON_TYPE(long double, unsigned long long, long double);
SOL_COMMON_TYPE(long double, float, long double);
SOL_COMMON_TYPE(long double, double, long double);
SOL_COMMON_TYPE(long double, long double, long double);


template <typename T1 = null_type
    , typename T2 = null_type
    , typename T3 = null_type
    , typename T4 = null_type
    , typename T5 = null_type
>
struct common_type;


template <typename T>
struct common_type<T, null_type, null_type, null_type, null_type>
{
    typedef T type;
};


template <typename T1, typename T2>
struct common_type<T1, T2, null_type, null_type, null_type>
{
    typedef typename common_type_<T1, T2>::type type;
};


template <typename T1, typename T2, typename T3>
struct common_type<T1, T2, T3, null_type, null_type>
{
    typedef typename common_type_<
        typename common_type_<T1, T2>::type, T3
    >::type type;
};


template <typename T1, typename T2, typename T3, typename T4>
struct common_type<T1, T2, T3, T4, null_type>
{
    typedef typename common_type_<
        typename common_type_<
            typename common_type_<T1, T2>::type, T3
        >::type, T4
    >::type type;
};


template <typename T1, typename T2, typename T3, typename T4, typename T5>
struct common_type
{
    typedef typename common_type_<
        typename common_type_<
            typename common_type_<
                typename common_type_<T1, T2>::type
                , T3
            >::type
            , T4
        >::type
        , T5
    >::type type;
};


#endif


}   // namespace sol


/// \}
#endif  // sol_utility_common_type_hpp_included
