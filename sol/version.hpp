#if !defined(sol_version_hpp_included)
#define sol_version_hpp_included


/**
 * \file sol/version.hpp
 * \brief Library version stuff
 * \author Sven Suursoho
 */


/**
 * Create packed version number
 *
 * To check required version, do
 * \code
 * #if (SOL_VERSION >= SOL_MAKE_VERSION(0,2,1))
 * \endcode
 */
#define SOL_MAKE_VERSION(major,minor,patch) \
    ((major << 16) | (minor << 8) | patch)


/// Library version string
#define SOL_VERSION_STR "0.1.0"


/// Packed numeric library version
#define SOL_VERSION SOL_MAKE_VERSION(0,1,0)


#endif  // sol_version_hpp_included
