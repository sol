#if !defined(sol_atomic_flag_hpp_included)
#define sol_atomic_flag_hpp_included
/// \addtogroup sol_atomic
/// \{


/**
 * \file sol/atomic/flag.hpp
 * \brief Basic atomic flag
 * \author Sven Suursoho
 *
 * \see http://www.intel.com/cd/ids/developer/asmo-na/eng/dc/threading/333935.htm
 */


#include <sol/atomic/operations.hpp>


namespace sol { namespace atomic {


/// Boolean atomic flag (basically spinlock with true=locked, false=unlocked)
class flag
{
public:

    /// Initialize flag to \a state
    flag (bool state = false)
        : state_(false)
    {}


    /// Return current flag state
    bool
    test () const volatile
    {
        return load(&state_, acquire);
    }


    /// Atomically set flag to true and return old value
    bool
    test_and_set () volatile
    {
        return swap(&state_, true, acq_rel);
    }


    /**
     * Set flag
     *
     * Operation is tried in spin loop until flag is set by current thread or
     * backoff functor returns true. See sol/atomic/backoff.hpp for more
     * information.
     *
     * \returns true if setting flag succeeded or false otherwise.
     */
    template <typename Backoff>
    bool
    set (Backoff const &backoff) volatile;


    /// Try once to set flag. Returns true on success and false otherwise
    bool
    try_set () volatile
    {
        return test_and_set() == false;
    }


    /// Clear flag (set value to false)
    void
    clear () volatile
    {
        store(&state_, false, release);
    }


private:

    bool volatile state_;
};


template <typename Backoff>
bool
flag::set (Backoff const &backoff) volatile
{
    unsigned long iter_count(0);
    while (true) {
        if ((void volatile *)state_ == 0) {
            if (test_and_set() == false) {
                // success, acquired flag
                return true;
            }
        }
        else if (backoff(++iter_count)) {
            // ordered to back off, failure
            return false;
        }
    }

    // never reached
    return false;
}


}}  // namespace sol::atomic


/// \}
#endif  // sol_atomic_flag_hpp_included
