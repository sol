#if !defined(sol_config_os_posix_hpp_included)
#define sol_config_os_posix_hpp_included
/// \addtogroup sol_config
/// \{


/**
 * \file sol/config/os/posix.hpp
 * \brief Posix-specific environment configuration
 * \author Sven Suursoho
 */


//
// OS name already set by our includer
//

#define SOL_OS_POSIX 1


// see "man unistd.h(0p)"
#if defined(SOL_HAS_UNISTD_H)
    #include <unistd.h>

    #if !defined(_POSIX_VERSION)
        #error Symbol _POSIX_VERSION not defined by unistd.h
    #endif

    // stdint.h
    #if _POSIX_VERSION > 200100L
        #define SOL_HAS_STDINT_H
    #endif
#endif


/// \}
#endif  // sol_config_os_posix_hpp_included
