#if !defined(sol_date_time_duration_hpp_included)
#define sol_date_time_duration_hpp_included
/// \addtogroup sol_date_time
/// \{


/**
 * \file sol/date_time/duration.hpp
 * \brief Length of time unattached to a any time point.
 * \author Sven Suursoho
 */


namespace sol { namespace date_time {


/// Common duration type, ancestor class for more specific duration
template <
    typename FinalType
    , typename TickType
    , int TicksPerSecond
    , int SecondsPerTick
>
class duration_type
{
public:

    /// Count type at the resolution of duration type
    typedef TickType tick_type;


    /// Number of ticks per second
    static tick_type const ticks_per_second = TicksPerSecond;


    /// Number of seconds per tick
    static tick_type const seconds_per_tick = SecondsPerTick;


    /// True if resolution is less than 1 second
    static bool const is_subsecond = TicksPerSecond > SecondsPerTick;


    /// \internal Helper type definition
    typedef duration_type<
        FinalType, TickType, TicksPerSecond, SecondsPerTick
    > this_type;


    /// Construct duration object (defaulting to count of 0)
    duration_type (tick_type d = 0)
        : count_(d)
    {}


    /// Return the count at the resolution of the time duration type
    tick_type
    get_count () const
    {
        return count_;
    }


    /// Return this + that
    FinalType
    operator+ (FinalType const &that) const
    {
        return FinalType(count_ + that.count_);
    }


    /// Return this += that
    FinalType &
    operator+= (FinalType const &that)
    {
        count_ += that.count_;
        return *static_cast<FinalType *>(this);
    }


    /// Return this - that
    FinalType
    operator- (FinalType const &that) const
    {
        return FinalType(count_ - that.count_);
    }


    /// Return this -= that
    FinalType &
    operator-= (FinalType const &that)
    {
        count_ -= that.count_;
        return *static_cast<FinalType *>(this);
    }


    /// Return this/divisor
    FinalType
    operator/ (int divisor) const
    {
        return FinalType(count_/divisor);
    }


    /// Return this/=divisor
    FinalType &
    operator/= (int divisor)
    {
        count_ /= divisor;
        return *static_cast<FinalType *>(this);
    }


    /// Return this*divisor
    FinalType
    operator* (int multiplier) const
    {
        return FinalType(count_*multiplier);
    }


    /// Return this*=divisor
    FinalType &
    operator*= (int multiplier)
    {
        count_ *= multiplier;
        return *static_cast<FinalType *>(this);
    }


    /// Return negate of this
    FinalType
    operator- () const
    {
        return FinalType(-count_);
    }


    /// Return true if this == that
    bool
    operator== (this_type const &that) const
    {
        return count_ == that.count_;
    }


    /// Return true if this != that
    bool
    operator!= (this_type const &that) const
    {
        return count_ != that.count_;
    }


    /// Return true if this > that
    bool
    operator> (this_type const &that) const
    {
        return count_ > that.count_;
    }


    /// Return true if this >= that
    bool
    operator>= (this_type const &that) const
    {
        return count_ >= that.count_;
    }


    /// Return true if this < that
    bool
    operator< (this_type const &that) const
    {
        return count_ < that.count_;
    }


    /// Return true if this <= that
    bool
    operator<= (this_type const &that) const
    {
        return count_ <= that.count_;
    }


protected:

    tick_type count_;
};


/// Duration in nanoseconds
class nanoseconds
    : public duration_type<nanoseconds, long long, 1000000000, 0>
{
public:

    nanoseconds (tick_type d = 0)
        : this_type(d)
    {}
};


/// Duration in microseconds
class microseconds
    : public duration_type<microseconds, long long, 1000000, 0>
{
public:

    microseconds (tick_type d = 0)
        : this_type(d)
    {}


    operator nanoseconds () const
    {
        return count_*(nanoseconds::ticks_per_second/ticks_per_second);
    }
};


/// Duration in milliseconds
class milliseconds
    : public duration_type<milliseconds, long long, 1000, 0>
{
public:

    milliseconds (tick_type d = 0)
        : this_type(d)
    {}


    operator nanoseconds () const
    {
        return count_*(nanoseconds::ticks_per_second/ticks_per_second);
    }


    operator microseconds () const
    {
        return count_*(microseconds::ticks_per_second/ticks_per_second);
    }
};


/// Duration in seconds
class seconds
    : public duration_type<seconds, long long, 1, 1>
{
public:

    seconds (tick_type d = 0)
        : this_type(d)
    {}


    operator nanoseconds () const
    {
        return count_*(nanoseconds::ticks_per_second/ticks_per_second);
    }


    operator microseconds () const
    {
        return count_*(microseconds::ticks_per_second/ticks_per_second);
    }


    operator milliseconds () const
    {
        return count_*(milliseconds::ticks_per_second/ticks_per_second);
    }
};


/// Duration in minutes
class minutes
    : public duration_type<minutes, long long, 0, 60>
{
public:

    minutes (tick_type d = 0): this_type(d)
    {}


    operator nanoseconds () const
    {
        return count_*seconds_per_tick*nanoseconds::ticks_per_second;
    }


    operator microseconds () const
    {
        return count_*seconds_per_tick*microseconds::ticks_per_second;
    }


    operator milliseconds () const
    {
        return count_*seconds_per_tick*milliseconds::ticks_per_second;
    }


    operator seconds () const
    {
        return count_*seconds_per_tick;
    }
};


/// Duration in hours
class hours
    : public duration_type<hours, long long, 0, 3600>
{
public:

    hours (tick_type d = 0)
        : this_type(d)
    {}


    operator nanoseconds () const
    {
        return count_*seconds_per_tick*nanoseconds::ticks_per_second;
    }


    operator microseconds () const
    {
        return count_*seconds_per_tick*microseconds::ticks_per_second;
    }


    operator milliseconds () const
    {
        return count_*seconds_per_tick*milliseconds::ticks_per_second;
    }


    operator seconds () const
    {
        return count_*seconds_per_tick;
    }


    operator minutes () const
    {
        return count_*(seconds_per_tick/minutes::seconds_per_tick);
    }
};


}}  // namespace sol::date_time


/// \}
#endif  // sol_date_time_duration_hpp_included
