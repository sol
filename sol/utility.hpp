#if !defined(sol_utility_hpp_included)
#define sol_utility_hpp_included
/// \addtogroup sol
/// \{


/**
 * \file sol/utility.hpp
 * \brief Generic utilities
 *
 * \note Utilities do not have separate namespace
 *
 * Header(s):
 *  - sol/utility/common_type.hpp
 *  - sol/utility/conditional.hpp
 *  - sol/utility/integral_constant.hpp
 */


#include <sol/utility/common_type.hpp>
#include <sol/utility/conditional.hpp>
#include <sol/utility/integral_constant.hpp>


/// \}
#endif  // sol_utility_hpp_included
