#if !defined(sol_date_time_utc_time_hpp_included)
#define sol_date_time_utc_time_hpp_included
/// \addtogroup sol_date_time
/// \{


/**
 * \file sol/date_time/utc_time.hpp
 * \brief UTC time similar to time_t but with nanosecond resolution
 * \author Sven Suursoho
 */


#include <sol/date_time/duration.hpp>


namespace sol { namespace date_time {


/**
 * UTC time similar to std::time_t but with higher resolution
 * (internally nanosecond, but depending on operating system and hardware
 * platform it is probably lower)
 */
class utc_time
{
public:

    /// Storage type for number of nanoseconds since Epoch
    typedef nanoseconds::tick_type tick_type;


    /// Number of ticks per second
    static tick_type const ticks_per_second = nanoseconds::ticks_per_second;


    /// Number of seconds per tick
    static tick_type const seconds_per_tick = nanoseconds::seconds_per_tick;


    /// True if resolution is less than 1 second
    static bool const is_subsecond = nanoseconds::is_subsecond;


    /// Zero time (i.e. Epoch)
    utc_time ()
        : time_(0)
    {}


    /// Dtor
    ~utc_time ()
    {}


    /// Return number of seconds since Epoch
    time_t
    seconds_since_epoch () const
    {
        return time_/nanoseconds::ticks_per_second;
    }


    /// Return number of nanoseconds since Epoch
    nanoseconds
    nanoseconds_since_epoch () const
    {
        return time_;
    }


    /// Return true if this == that
    bool
    operator== (utc_time const &that) const
    {
        return time_ == that.time_;
    }


    /// Return true if this != that
    bool
    operator!= (utc_time const &that) const
    {
        return time_ != that.time_;
    }


    /// Return true if this > that
    bool
    operator> (utc_time const &that) const
    {
        return time_ > that.time_;
    }


    /// Return true if this >= that
    bool
    operator>= (utc_time const &that) const
    {
        return time_ >= that.time_;
    }


    /// Return true if this < that
    bool
    operator< (utc_time const &that) const
    {
        return time_ < that.time_;
    }


    /// Return true if this <= that
    bool
    operator<= (utc_time const &that) const
    {
        return time_ <= that.time_;
    }


    /// Return duration between this and that
    nanoseconds
    operator- (utc_time const &that) const
    {
        return nanoseconds(time_ - that.time_);
    }


    /// Shift time point by \a d
    template <typename DurationType>
    utc_time
    operator+ (DurationType const &d) const
    {
        nanoseconds ns(d);
        return utc_time(time_ + ns.get_count());
    }


    /// Shift this time point by \a d
    template <typename DurationType>
    utc_time &
    operator+= (DurationType const &d)
    {
        nanoseconds ns(d);
        time_ += ns.get_count();
        return *this;
    }


    /// Shift time point by \a d
    template <typename DurationType>
    utc_time
    operator- (DurationType const &d) const
    {
        nanoseconds ns(d);
        return utc_time(time_ - ns.get_count());
    }


    /// Shift this time point by \a d
    template <typename DurationType>
    utc_time &
    operator-= (DurationType const &d)
    {
        nanoseconds ns(d);
        time_ -= ns.get_count();
        return *this;
    }


private:

    friend class hiresolution_clock;

    utc_time (nanoseconds::tick_type t)
        : time_(t)
    {}

    nanoseconds::tick_type time_;
};


}}  // namespace sol::date_time


/// \}
#endif  // sol_date_time_utc_time_hpp_included
