#if !defined(sol_static_assert_hpp_included)
#define sol_static_assert_hpp_included
/// \addtogroup sol
/// \{


/**
 * \file sol/static_assert.hpp
 * \brief Temporary helper static_assert() until it will be standardized
 * \author Sven Suursoho
 *
 * \see http://www.open-std.org/JTC1/SC22/WG21/docs/papers/2004/n1720.html
 */


#include <sol/config.hpp>
#if !defined(SOL_HAS_STATIC_ASSERT)
    #include <sol/preprocessor/join.hpp>

    /// Static compile-time assertion -- check if \a condition is true
    #define static_assert(condition,msg) \
        enum { \
            SOL_JOIN(SOL_STATIC_ASSERTION_FAILURE_ON_LINE_,__LINE__) = \
                1/int(bool(condition)) \
        }

#endif  // SOL_HAS_STATIC_ASSERT


/// \}
#endif  // sol_static_assert_hpp_included
