#if !defined(sol_date_time_bits_hiresolution_clock_hpp_included)
#define sol_date_time_bits_hiresolution_clock_hpp_included


#include <sys/time.h>


namespace sol { namespace date_time {


inline utc_time
hiresolution_clock::universal_time ()
{
  timeval tv;
  gettimeofday(&tv, 0);

  nanoseconds::tick_type time_since_epoch(
      tv.tv_sec*nanoseconds::ticks_per_second + tv.tv_usec*1000
  );

  return utc_time(time_since_epoch);
}


}}  // namespace sol::date_time


#endif  // sol_date_time_bits_hiresolution_clock_hpp_included
