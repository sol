#if !defined(sol_preprocessor_text_hpp_included)
#define sol_preprocessor_text_hpp_included
/// \addtogroup sol_preprocessor
/// \{


/**
 * \file sol/preprocessor/text.hpp
 * \author Sven Suursoho
 */


/// Turn preprocessor symbol into text
#define SOL_TEXT(s) SOL_TEXT0(s)

/// \internal
#define SOL_TEXT0(s) #s


/// \}
#endif  // sol_preprocessor_text_hpp_included
