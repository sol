#if !defined(sol_config_hpp_included)
#define sol_config_hpp_included
/// \defgroup sol_config
/// \ingroup sol
/// \{


/**
 * \file sol/config.hpp
 * \brief Project and environment configuration
 * \author Sven Suursoho
 */


#include <sol/config/user.hpp>
#include <sol/config/compiler.hpp>
#include <sol/config/arch.hpp>
#include <sol/config/os.hpp>


namespace sol {


/// System information
class sys_info
{
public:

    /// Compiler name
    static char const * const compiler_name;


    /// Architecture name
    static char const * const arch_name;


    /// Operating system name
    static char const * const os_name;


    /// Byte ordering types
    enum endian_type
    {
        big_endian = SOL_BIG_ENDIAN,
        little_endian = SOL_LITTLE_ENDIAN,
        byte_order = SOL_BYTE_ORDER
    };
};


}   // namespace sol


/// \}
#endif  // sol_config_hpp_included
