#if !defined(sol_math_ratio_hpp_included)
#define sol_math_ratio_hpp_included
/// \addtogroup sol_math
/// \{


/**
 * \file sol/math/ratio.hpp
 * \brief Compute rational values at compile time
 *
 * \see http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2661.htm#ratio
 *
 * \todo ratio comparison
 */


#include <climits>
#include <sol/cstdint.hpp>
#include <sol/math/abs.hpp>
#include <sol/math/gcd.hpp>
#include <sol/math/sign.hpp>
#include <sol/static_assert.hpp>
#include <sol/utility/integral_constant.hpp>


namespace sol { namespace math {


namespace bits {


#if !defined(INTMAX_MAX)
    #define INTMAX_MAX INT_MAX
#endif


template <intmax_t A, intmax_t B, bool IsPositiveB>
struct add_overflow
    : integral_constant<bool, (A <= INTMAX_MAX - B)>
{};


template <intmax_t A, intmax_t B>
struct add_overflow<A, B, false>
    : integral_constant<bool, (A >= -INTMAX_MAX - B)>
{};


template <intmax_t A, intmax_t B>
struct add_
{
    static_assert((add_overflow<A, B, B >= 0>::value == true), "overflow");
    static intmax_t const value = A + B;
};


template <intmax_t A, intmax_t B>
struct multiply_
{
    static uintmax_t const c = 1UL << (4*sizeof(intmax_t));

    static uintmax_t const a0 = abs<A>::value%c;
    static uintmax_t const a1 = abs<A>::value/c;
    static uintmax_t const b0 = abs<B>::value%c;
    static uintmax_t const b1 = abs<B>::value/c;

    static_assert(a1 == 0 || b1 == 0, "overflow");
    static_assert((a0*b1 + b0*a1) < (c >> 1), "overflow");
    static_assert(b0*a0 <= INTMAX_MAX, "overflow");
    static_assert((a0*b1 + b0*a1)*c <= INTMAX_MAX - b0*a0, "overflow");

    static intmax_t const value = A*B;
};


}   // namespace bits


/// Rational value representation (calculated during compile-time)
template <intmax_t N, intmax_t D=1>
struct ratio
{
    static_assert(D != 0, "division by 0");

    /// numerator
    static intmax_t const num = N*sign<D>::value/gcd<N, D>::value;

    /// denominator
    static intmax_t const den = abs<D>::value/gcd<N, D>::value;
};


/// rational arithmetic: addition
template <typename R1, typename R2>
struct ratio_add
{
private:

    enum { g = gcd<R1::den, R2::den>::value };


public:

    /// result
    typedef ratio<
        bits::add_<
            bits::multiply_<R1::num, R2::den/g>::value
            , bits::multiply_<R2::num, R1::den/g>::value
        >::value
        , bits::multiply_<R1::den, R2::den/g>::value
    > type;
};


/// rational arithmetic: subtraction
template <typename R1, typename R2>
struct ratio_subtract
{
    /// result
    typedef typename ratio_add<
        R1
        , ratio<-R2::num, R2::den>
    >::type type;
};


/// rational arithmetic: multiplication
template <typename R1, typename R2>
struct ratio_multiply
{
private:

    enum {
        g1 = gcd<R1::num, R2::den>::value
        , g2 = gcd<R2::num, R1::den>::value
    };

public:

    /// result
    typedef ratio<
        bits::multiply_<R1::num/g1, R2::num/g2>::value
        , bits::multiply_<R1::den/g2, R2::den/g1>::value
    > type;
};


/// rational arithmetic: division
template <typename R1, typename R2>
struct ratio_divide
{
    static_assert(R2::num != 0, "division by 0");

    /// result
    typedef typename ratio_multiply<
        R1
        , ratio<R2::den, R2::num>
    >::type type;
};


/// Comparison: R1 == R2
template <typename R1, typename R2>
struct ratio_equal
    : integral_constant<bool, R1::num == R2::num && R1::den == R2::den>
{};


/// Comparison: R1 != R2
template <typename R1, typename R2>
struct ratio_not_equal
    : integral_constant<bool, !ratio_equal<R1, R2>::value>
{};


/// Comparison: R1 < R2
template <typename R1, typename R2>
struct ratio_less
    : integral_constant<bool
        , bits::multiply_<R1::num, R2::den>::value
            < bits::multiply_<R2::num, R1::den>::value
      >
{};


/// Comparison: R1 <= R2
template <typename R1, typename R2>
struct ratio_less_equal
    : integral_constant<bool, !ratio_less<R2, R1>::value>
{};


/// Comparison: R1 > R2
template <typename R1, typename R2>
struct ratio_greater
    : integral_constant<bool, ratio_less<R2, R1>::value>
{};


/// Comparison: R1 >= R2
template <typename R1, typename R2>
struct ratio_greater_equal
    : integral_constant<bool, !ratio_less<R1, R2>::value>
{};


/// Convenience SI units
/// \{
typedef ratio<1, 1000000000000000000> atto;
typedef ratio<1,    1000000000000000> femto;
typedef ratio<1,       1000000000000> pico;
typedef ratio<1,          1000000000> nano;
typedef ratio<1,             1000000> micro;
typedef ratio<1,                1000> milli;
typedef ratio<1,                 100> centi;
typedef ratio<1,                  10> deci;
typedef ratio<                 10, 1> deca;
typedef ratio<                100, 1> hecto;
typedef ratio<               1000, 1> kilo;
typedef ratio<            1000000, 1> mega;
typedef ratio<         1000000000, 1> giga;
typedef ratio<      1000000000000, 1> tera;
typedef ratio<   1000000000000000, 1> peta;
typedef ratio<1000000000000000000, 1> exa;
/// \}


}}  // namespace sol::math


/// \}
#endif  // sol_math_ratio_hpp_included
