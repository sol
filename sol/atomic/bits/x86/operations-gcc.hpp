#if !defined(sol_atomic_bits_x86_operations_gcc_hpp_included)
#define sol_atomic_bits_x86_operations_gcc_hpp_included


namespace sol { namespace atomic {


// load {{{

template <typename T>
bool const load_op<T>::lock_free = true;
template <typename T>
bool const load_op<T>::wait_free = true;


template <typename T>
inline T
load (T volatile const *a, relaxed_type)
{
    return load(a, seq_cst);
}


template <typename T>
inline T *
load (T * volatile const *a, relaxed_type)
{
    return load(a, seq_cst);
}


template <typename T>
inline T
load (T volatile const *a, acquire_type)
{
    return load(a, seq_cst);
}


template <typename T>
inline T *
load (T * volatile const *a, acquire_type)
{
    return load(a, seq_cst);
}


template <typename T>
inline T
load (T volatile const *a, seq_cst_type)
{
    return *a;
}


template <typename T>
inline T *
load (T * volatile const *a, seq_cst_type)
{
    return *(T **)a;
}

// }}}


// store {{{

template <typename T>
bool const store_op<T>::lock_free = true;
template <typename T>
bool const store_op<T>::wait_free = true;


template <typename T>
inline T
store (T volatile *a, T v, relaxed_type)
{
    return store(a, v, seq_cst);
}


template <typename T>
inline T *
store (T * volatile *a, T const *v, relaxed_type)
{
    return store(a, v, seq_cst);
}


template <typename T>
inline T
store (T volatile *a, T v, release_type)
{
    return store(a, v, seq_cst);
}


template <typename T>
inline T *
store (T * volatile *a, T const *v, release_type)
{
    return store(a, v, seq_cst);
}


template <typename T>
inline T
store (T volatile *a, T v, seq_cst_type)
{
    return (*a = v);
}


template <typename T>
inline T *
store (T * volatile *a, T const *v, seq_cst_type)
{
    return (T *)(*a = v);
}

// }}}


// swap {{{

template <typename T>
bool const swap_op<T>::lock_free = true;
template <typename T>
bool const swap_op<T>::wait_free = true;


/// Store \a v into \a a (unordered). Return old value of \a a
template <typename T>
inline T
swap (T volatile *a, T v, relaxed_type)
{
    return swap(a, v, seq_cst);
}


/// Store \a v into \a a (unordered). Return old value of \a a
template <typename T>
inline T *
swap (T * volatile *a, T const *v, relaxed_type)
{
    return swap(a, v, seq_cst);
}


/// Store \a v into \a a (acquire). Return old value of \a a
template <typename T>
inline T
swap (T volatile *a, T v, acquire_type)
{
    return swap(a, v, seq_cst);
}


/// Store \a v into \a a (acquire). Return old value of \a a
template <typename T>
inline T *
swap (T * volatile *a, T const *v, acquire_type)
{
    return swap(a, v, seq_cst);
}


/// Store \a v into \a a (release). Return old value of \a a
template <typename T>
inline T
swap (T volatile *a, T v, release_type)
{
    return swap(a, v, seq_cst);
}


/// Store \a v into \a a (release). Return old value of \a a
template <typename T>
inline T *
swap (T * volatile *a, T const *v, release_type)
{
    return swap(a, v, seq_cst);
}


/// Store \a v into \a a (ordered). Return old value of \a a
template <typename T>
inline T
swap (T volatile *a, T v, acq_rel_type)
{
    return swap(a, v, seq_cst);
}


/// Store \a v into \a a (ordered). Return old value of \a a
template <typename T>
inline T *
swap (T * volatile *a, T const *v, acq_rel_type)
{
    return swap(a, v, seq_cst);
}


/// Store \a v into \a a (sequentially-consistent). Return old value of \a a
template <typename T>
inline T
swap (T volatile *a, T v, seq_cst_type)
{
    /*
    T old(*a);
    *a = v;
    return old;
    */
    T old;
    asm volatile (
        "xchg %0, %1"
        : "=r" (old), "=m" (*a)
        : "0" (v), "m" (*a)
        : "memory"
    );
    return old;
}


/// Store \a v into \a a (sequentially-consistent). Return old value of \a a
template <typename T>
inline T *
swap (T * volatile *a, T const *v, seq_cst_type)
{
    /*
    T *old(*(T **)a);
    *a = v;
    return old;
    */
    T *old;
    asm volatile (
        "xchg %0, %1"
        : "=r" (old), "=m" (*a)
        : "0" (v), "m" (*a)
        : "memory"
    );
    return old;
}


// }}}


// compare_swap {{{

template <typename T>
bool const compare_swap_op<T>::lock_free = true;
template <typename T>
bool const compare_swap_op<T>::wait_free = true;


template <typename T>
inline bool
compare_swap (T volatile *a, T e, T v, relaxed_type)
{
    return compare_swap(a, e, v, seq_cst);
}


template <typename T>
inline bool
compare_swap (T * volatile *a, T const *e, T const *v, relaxed_type)
{
    return compare_swap(a, e, v, seq_cst);
}


template <typename T>
inline bool
compare_swap (T volatile *a, T e, T v, acquire_type)
{
    return compare_swap(a, e, v, seq_cst);
}


template <typename T>
inline bool
compare_swap (T * volatile *a, T const *e, T const *v, acquire_type)
{
    return compare_swap(a, e, v, seq_cst);
}


template <typename T>
inline bool
compare_swap (T volatile *a, T e, T v, release_type)
{
    return compare_swap(a, e, v, seq_cst);
}


template <typename T>
inline bool
compare_swap (T * volatile *a, T const *e, T const *v, release_type)
{
    return compare_swap(a, e, v, seq_cst);
}


template <typename T>
inline bool
compare_swap (T volatile *a, T e, T v, acq_rel_type)
{
    return compare_swap(a, e, v, seq_cst);
}


template <typename T>
inline bool
compare_swap (T * volatile *a, T const *e, T const *v, acq_rel_type)
{
    return compare_swap(a, e, v, seq_cst);
}


template <typename T>
inline bool
compare_swap (T volatile *a, T e, T v, seq_cst_type)
{
    /*
    if (*a == e) {
        *a = v;
        return true;
    }
    return false;
    */
    bool result;
    asm volatile (
        "lock; cmpxchg %3, %0; setz %1"
        : "=m" (*a), "=a" (result)
        : "m" (*a), "q" (v), "a" (e)
        : "memory"
    );
    return result;
}


template <typename T>
inline bool
compare_swap (T * volatile *a, T const *e, T const *v, seq_cst_type)
{
    /*
    if (*a == e) {
        *a = v;
        return true;
    }
    return false;
    */
    bool result;
    asm volatile (
        "lock; cmpxchg %3, %0; setz %1"
        : "=m" (*a), "=a" (result)
        : "m" (*a), "q" (v), "a" (e)
        : "memory"
    );
    return result;
}

// }}}


// fetch_add {{{

template <typename T>
bool const fetch_add_op<T>::lock_free = true;
template <typename T>
bool const fetch_add_op<T>::wait_free = true;


template <typename T>
inline T
fetch_add (T volatile *a, T v, relaxed_type)
{
    return fetch_add(a, v, seq_cst);
}


template <typename T>
inline T *
fetch_add (T * volatile *a, ptrdiff_t v, relaxed_type)
{
    return fetch_add(a, v, seq_cst);
}


template <typename T>
inline T
fetch_add (T volatile *a, T v, acquire_type)
{
    return fetch_add(a, v, seq_cst);
}


template <typename T>
inline T *
fetch_add (T * volatile *a, ptrdiff_t v, acquire_type)
{
    return fetch_add(a, v, seq_cst);
}


template <typename T>
inline T
fetch_add (T volatile *a, T v, release_type)
{
    return fetch_add(a, v, seq_cst);
}


template <typename T>
inline T *
fetch_add (T * volatile *a, ptrdiff_t v, release_type)
{
    return fetch_add(a, v, seq_cst);
}


template <typename T>
inline T
fetch_add (T volatile *a, T v, acq_rel_type)
{
    return fetch_add(a, v, seq_cst);
}


template <typename T>
inline T *
fetch_add (T * volatile *a, ptrdiff_t v, acq_rel_type)
{
    return fetch_add(a, v, seq_cst);
}


template <typename T>
inline T
fetch_add (T volatile *a, T v, seq_cst_type)
{
    /*
    T old(*a);
    *a += v;
    return old;
    */
    T old;
    asm volatile (
        "lock; xadd %0, %1"
        : "=r" (old), "=m" (*a)
        : "0" (v), "m" (*a)
        : "memory"
    );
    return old;
}


template <typename T>
inline T *
fetch_add (T * volatile *a, ptrdiff_t v, seq_cst_type)
{
    /*
    T *old(*(T **)a);
    *a += v;
    return old;
    */
    v *= sizeof(**a);
    void *old;
    asm volatile (
        "lock; xadd %0, %1"
        : "=r" (old), "=m" (*a)
        : "0" (v), "m" (*a)
        : "memory"
    );
    return (T *)old;
}

// }}}


// fetch_sub {{{

template <typename T>
bool const fetch_sub_op<T>::lock_free = true;
template <typename T>
bool const fetch_sub_op<T>::wait_free = true;


template <typename T>
inline T
fetch_sub (T volatile *a, T v, relaxed_type)
{
    return fetch_sub(a, v, seq_cst);
}


template <typename T>
inline T *
fetch_sub (T * volatile *a, ptrdiff_t v, relaxed_type)
{
    return fetch_sub(a, v, seq_cst);
}


template <typename T>
inline T
fetch_sub (T volatile *a, T v, acquire_type)
{
    return fetch_sub(a, v, seq_cst);
}


template <typename T>
inline T *
fetch_sub (T * volatile *a, ptrdiff_t v, acquire_type)
{
    return fetch_sub(a, v, seq_cst);
}


template <typename T>
inline T
fetch_sub (T volatile *a, T v, release_type)
{
    return fetch_sub(a, v, seq_cst);
}


template <typename T>
inline T *
fetch_sub (T * volatile *a, ptrdiff_t v, release_type)
{
    return fetch_sub(a, v, seq_cst);
}


template <typename T>
inline T
fetch_sub (T volatile *a, T v, acq_rel_type)
{
    return fetch_sub(a, v, seq_cst);
}


template <typename T>
inline T *
fetch_sub (T * volatile *a, ptrdiff_t v, acq_rel_type)
{
    return fetch_sub(a, v, seq_cst);
}


template <typename T>
inline T
fetch_sub (T volatile *a, T v, seq_cst_type)
{
    return fetch_add(a, T(-v), seq_cst);
}


template <typename T>
inline T *
fetch_sub (T * volatile *a, ptrdiff_t v, seq_cst_type)
{
    return fetch_add(a, ptrdiff_t(-v), seq_cst);
}

// }}}


// fetch_and {{{

template <typename T>
bool const fetch_and_op<T>::lock_free = true;
template <typename T>
bool const fetch_and_op<T>::wait_free = false;


template <typename T>
inline T
fetch_and (T volatile *a, T v, relaxed_type)
{
    return fetch_and(a, v, seq_cst);
}


template <typename T>
inline T
fetch_and (T volatile *a, T v, acquire_type)
{
    return fetch_and(a, v, seq_cst);
}


template <typename T>
inline T
fetch_and (T volatile *a, T v, release_type)
{
    return fetch_and(a, v, seq_cst);
}


template <typename T>
inline T
fetch_and (T volatile *a, T v, acq_rel_type)
{
    return fetch_and(a, v, seq_cst);
}


template <typename T>
inline T
fetch_and (T volatile *a, T v, seq_cst_type)
{
    /*
    T old(*a);
    *a &= v;
    return old;
    */
    T old;
    do {
        old = *a;
    } while (!compare_swap(a, old, T(old & v), seq_cst));
    return old;
}

// }}}


// fetch_or {{{

template <typename T>
bool const fetch_or_op<T>::lock_free = true;
template <typename T>
bool const fetch_or_op<T>::wait_free = false;


template <typename T>
inline T
fetch_or (T volatile *a, T v, relaxed_type)
{
    return fetch_or(a, v, seq_cst);
}


template <typename T>
inline T
fetch_or (T volatile *a, T v, acquire_type)
{
    return fetch_or(a, v, seq_cst);
}


template <typename T>
inline T
fetch_or (T volatile *a, T v, release_type)
{
    return fetch_or(a, v, seq_cst);
}


template <typename T>
inline T
fetch_or (T volatile *a, T v, acq_rel_type)
{
    return fetch_or(a, v, seq_cst);
}


template <typename T>
inline T
fetch_or (T volatile *a, T v, seq_cst_type)
{
    /*
    T old(*a);
    *a |= v;
    return old;
    */
    T old;
    do {
        old = *a;
    } while (!compare_swap(a, old, T(old | v), seq_cst));
    return old;
}

// }}}


// fetch_xor {{{

template <typename T>
bool const fetch_xor_op<T>::lock_free = true;
template <typename T>
bool const fetch_xor_op<T>::wait_free = false;


template <typename T>
inline T
fetch_xor (T volatile *a, T v, relaxed_type)
{
    return fetch_xor(a, v, seq_cst);
}


template <typename T>
inline T
fetch_xor (T volatile *a, T v, acquire_type)
{
    return fetch_xor(a, v, seq_cst);
}


template <typename T>
inline T
fetch_xor (T volatile *a, T v, release_type)
{
    return fetch_xor(a, v, seq_cst);
}


template <typename T>
inline T
fetch_xor (T volatile *a, T v, acq_rel_type)
{
    return fetch_xor(a, v, seq_cst);
}


template <typename T>
inline T
fetch_xor (T volatile *a, T v, seq_cst_type)
{
    /*
    T old(*a);
    *a ^= v;
    return old;
    */
    T old;
    do {
        old = *a;
    } while (!compare_swap(a, old, T(old ^ v), seq_cst));
    return old;
}

// }}}


}}  // namespace sol::atomic


// let sol/atomic/bits/generic/operations.hpp implement missing as locked
// yes, we know it is x86 but check it anyway as x86_64 includes this header
#if defined(SOL_ARCH_X86)
    #define SOL_ATOMIC_no_long_long
    #include <sol/atomic/bits/generic/operations.hpp>
#endif


#endif  // sol_atomic_bits_x86_operations_gcc_hpp_included
