#if !defined(sol_config_arch_hpp_included)
#define sol_config_arch_hpp_included
/// \addtogroup sol_config
/// \{


/**
 * \file sol/config/arch.hpp
 * \brief Architecture-specific environment configuration
 * \author Sven Suursoho
 */


// byte ordering
#define SOL_BIG_ENDIAN 0x3412
#define SOL_LITTLE_ENDIAN 0x1234


//
// detect
//

#if defined(SOL_ARCH_CONFIG)
    // already have user specified architecture config

#elif defined(__x86_64__) || defined(__x86_64) \
    || defined(__amd64__) || defined(__amd64)
    // x86-64
    #define SOL_ARCH_CONFIG "sol/config/arch/x86_64.hpp"

#elif defined(__i386__) || defined(__i386) || defined(i386)
    // x86
    #define SOL_ARCH_CONFIG "sol/config/arch/x86.hpp"

#else
    #error "Unknown architecture"
#endif


//
// include
//

#if defined(SOL_ARCH_CONFIG)
    #include SOL_ARCH_CONFIG
#endif


//
// check for required stuff
//

#if !defined(SOL_ARCH_NAME)
    #error "SOL_ARCH_NAME not detected"
#endif

#if !defined(SOL_BYTE_ORDER)
    #error "SOL_BYTE_ORDER not detected"
#endif


/// \}
#endif  // sol_config_arch_hpp_included
