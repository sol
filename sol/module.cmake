# note: headers belonging to module will be listed by corresponding module.cmake


sol_headers(
    cstdint.hpp
    noncopyable.hpp
    static_assert.hpp
    version.hpp
)
