#if !defined(sol_preprocessor_join_hpp_included)
#define sol_preprocessor_join_hpp_included
/// \addtogroup sol_preprocessor
/// \{


/**
 * \file sol/preprocessor/join.hpp
 * \brief Join items
 * \author Sven Suursoho
 */


/// Join items \a s1 and \a s2
#define SOL_JOIN(s1,s2) SOL_JOIN0(s1,s2)

/// \internal
#define SOL_JOIN0(s1,s2) SOL_JOIN1(s1,s2)

/// \internal
#define SOL_JOIN1(s1,s2) s1##s2


/// \}
#endif  // sol_preprocessor_join_hpp_included
