#if !defined(sol_date_time_hiresolution_clock_hpp_included)
#define sol_date_time_hiresolution_clock_hpp_included
/// \addtogroup sol_date_time
/// \{


/**
 * \file sol/date_time/hiresolution_clock.hpp
 * \brief Provide access to operating system clock with resolution up to
 * nanosecond.
 * \author Sven Suursoho
 */


#include <sol/date_time/utc_time.hpp>


namespace sol { namespace date_time {


/// Provide access to operating system clock with resolution up to nanosecond.
/// Actual resolution depends on platform.
class hiresolution_clock
{
public:

    /// Return number of nanoseconds since Epoch
    static utc_time
    universal_time ();
};


}}  // namespace sol::date_time


#include <sol/date_time/bits/hiresolution_clock.hpp>


/// \}
#endif  // sol_date_time_hiresolution_clock_hpp_included
