#if !defined(sol_config_os_linux_hpp_included)
#define sol_config_os_linux_hpp_included
/// \addtogroup sol_config
/// \{


/**
 * \file sol/config/os/linux.hpp
 * \brief Linux-specific environment configuration
 * \author Sven Suursoho
 */


#include <sol/version.hpp>


//
// set OS name
//

#define SOL_OS_LINUX 1

#define SOL_OS_NAME "linux"


//
// glibc version
//

#include <cstdlib>
#define SOL_GLIBC_VERSION SOL_MAKE_VERSION(__GLIBC__,__GLIBC_MINOR__,0)


//
// cstdint | stdint.h | inttypes.h
//

#if SOL_GLIBC_VERSION >= SOL_MAKE_VERSION(2,1,0)
    #define SOL_HAS_STDINT_H
#endif


// Posix features
#define SOL_HAS_UNISTD_H
#include <sol/config/os/posix.hpp>


/// \}
#endif  // sol_config_os_linux_hpp_included
