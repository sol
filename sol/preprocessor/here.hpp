#if !defined(sol_preprocessor_here_hpp_included)
#define sol_preprocessor_here_hpp_included
/// \addtogroup sol_preprocessor
/// \{


/**
 * \file sol/preprocessor/here.hpp
 * \author Sven Suursoho
 */

#include <sol/preprocessor/text.hpp>


/// Turn preprocessor symbol into here
#define SOL_HERE __FILE__ ":" SOL_TEXT(__LINE__)


/// \}
#endif  // sol_preprocessor_here_hpp_included
