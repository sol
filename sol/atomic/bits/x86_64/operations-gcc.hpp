#if !defined(sol_atomic_bits_x86_64_operations_gcc_hpp_included)
#define sol_atomic_bits_x86_64_operations_gcc_hpp_included


// share code with x86 (except 64bit types, which fall back to generic impl)
#include <sol/atomic/bits/x86/operations-gcc.hpp>


#endif  // sol_atomic_bits_x86_64_operations_gcc_hpp_included
