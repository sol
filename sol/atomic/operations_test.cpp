#include <sol/test_main.cpp>
#include <sol/atomic/operations.hpp>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <limits>


namespace sol { namespace test {


namespace atomic = sol::atomic;


template <typename T>
void
test_load_store_swap (char const *type)
{
    sol_info((std::string("\t") + type).c_str());

    T volatile v(0);

    // load/store
    sol_equal(atomic::load(&v, atomic::acquire), T(0));
    sol_equal(atomic::store(&v, T(1), atomic::release), T(1));
    sol_equal(atomic::load(&v, atomic::acquire), T(1));

    // swap
    sol_equal(atomic::swap(&v, T(2), atomic::release), T(1));
    sol_equal(atomic::load(&v, atomic::acquire), T(2));

    // compare_swap
    sol_equal(atomic::compare_swap(&v, T(0), T(1), atomic::relaxed), false);
    sol_equal(atomic::load(&v, atomic::acquire), T(2));
    sol_equal(atomic::compare_swap(&v, T(2), T(1), atomic::release), true);
    sol_equal(atomic::load(&v, atomic::acquire), T(1));
}


template <typename T>
void
test_arithmetic (char const *type)
{
    sol_info((std::string("\t") + type).c_str());

    T const min_(std::numeric_limits<T>::min());
    T const max_(std::numeric_limits<T>::max());
    T test_points[] = { min_, min_/2, T(-2), T(-1), 0, 1, 2, max_/2, max_ };
    T *end(test_points + sizeof(test_points)/sizeof(test_points[0]));

    for (T *i = test_points;  i != end;  ++i) {
        T v(*i);
        T volatile av(*i);
        sol_equal(atomic::load(&av, atomic::acquire), v);

        { // fetch_add
            sol_equal(atomic::fetch_add(&av, T(1), atomic::acquire), v++);
            sol_equal(atomic::load(&av, atomic::acquire), v);
            sol_equal(atomic::fetch_add(&av, T(2), atomic::release), v);
            v += 2;
            sol_equal(atomic::load(&av, atomic::acquire), v);
        }

        { // fetch_sub
            sol_equal(atomic::fetch_sub(&av, T(1), atomic::acquire), v--);
            sol_equal(atomic::load(&av, atomic::acquire), v);
            sol_equal(atomic::fetch_sub(&av, T(2), atomic::release), v);
            v -= 2;
            sol_equal(atomic::load(&av, atomic::acquire), v);
        }

        T mask((*i)*7/3 + 1);

        { // fetch_and
            sol_equal(atomic::fetch_and(&av, mask, atomic::acquire), v);
            v &= mask;
            sol_equal(atomic::load(&av, atomic::acquire), v);
        }

        { // fetch_or
            mask <<= 1;
            sol_equal(atomic::fetch_or(&av, mask, atomic::acquire), v);
            v |= mask;
            sol_equal(atomic::load(&av, atomic::acquire), v);
        }

        { // fetch_xor
            mask >>= 1;
            sol_equal(atomic::fetch_xor(&av, mask, atomic::acquire), v);
            v ^= mask;
            sol_equal(atomic::load(&av, atomic::acquire), v);
        }
    }
}



template <typename T>
void
test_pointer (char const *type)
{
    sol_info((std::string("\t") + type).c_str());

    T s[] = { T('1'), T('2'), T('3'), T('\0') }, *v(s);
    T * volatile av(s);

    // load/store
    sol_equal(atomic::load(&av, atomic::acquire), v);
    sol_equal(atomic::store(&av, (T *)0, atomic::release), (T *)0);

    // swap
    sol_equal(atomic::swap(&av, v + 1, atomic::release), (T *)0);
    sol_equal(atomic::load(&av, atomic::acquire), v + 1);
    sol_equal(atomic::swap(&av, v, atomic::release), v + 1);
    sol_equal(atomic::load(&av, atomic::acquire), v);

    // compare_swap
    sol_equal(atomic::compare_swap(&av, v + 1, v, atomic::release), false);
    sol_equal(atomic::load(&av, atomic::acquire), v);
    sol_equal(atomic::compare_swap(&av, v, v + 1, atomic::release), true);
    sol_equal(atomic::load(&av, atomic::acquire), v + 1);

    // fetch_add
    sol_equal(atomic::fetch_add(&av, 1, atomic::acquire), v + 1);
    sol_equal(atomic::load(&av, atomic::acquire), v + 2);

    // fetch_sub
    sol_equal(atomic::fetch_sub(&av, 1, atomic::acquire), v + 2);
    sol_equal(atomic::load(&av, atomic::acquire), v + 1);
}


template <typename T>
void
print_traits (char const *type)
{
    std::cout << std::left << std::setw(19) << type

        // load
        << (atomic::load_op<T>::lock_free ? '+' : '-')
        << '/'
        << (atomic::load_op<T>::wait_free ? '+' : '-')

        // store
        << "   "
        << (atomic::store_op<T>::lock_free ? '+' : '-')
        << '/'
        << (atomic::store_op<T>::wait_free ? '+' : '-')

        // swap
        << "   "
        << (atomic::swap_op<T>::lock_free ? '+' : '-')
        << '/'
        << (atomic::swap_op<T>::wait_free ? '+' : '-')

        // compare_swap
        << "  "
        << (atomic::compare_swap_op<T>::lock_free ? '+' : '-')
        << '/'
        << (atomic::compare_swap_op<T>::wait_free ? '+' : '-')

        // fetch_add
        << "  "
        << (atomic::fetch_add_op<T>::lock_free ? '+' : '-')
        << '/'
        << (atomic::fetch_add_op<T>::wait_free ? '+' : '-')

        // fetch_sub
        << "  "
        << (atomic::fetch_sub_op<T>::lock_free ? '+' : '-')
        << '/'
        << (atomic::fetch_sub_op<T>::wait_free ? '+' : '-')

        // fetch_and
        << "  "
        << (atomic::fetch_and_op<T>::lock_free ? '+' : '-')
        << '/'
        << (atomic::fetch_and_op<T>::wait_free ? '+' : '-')

        // fetch_or
        << "  "
        << (atomic::fetch_or_op<T>::lock_free ? '+' : '-')
        << '/'
        << (atomic::fetch_or_op<T>::wait_free ? '+' : '-')

        // fetch_xor
        << "  "
        << (atomic::fetch_xor_op<T>::lock_free ? '+' : '-')
        << '/'
        << (atomic::fetch_xor_op<T>::wait_free ? '+' : '-')

        << std::endl;
}


int
run (std::vector<std::string> const &args)
{
    if (std::find(args.begin(), args.end(), std::string("--print-traits"))
        != args.end())
    {
        std::cout << "\natomic operations traits (lock_free/wait_free)"
            << "\ntype               "
               "load  store swp  cas  add  sub  and  or   xor"
            << std::endl;

        // no testing, just print operation traits
        print_traits<bool>("bool");
        print_traits<wchar_t>("wchar_t");
        print_traits<signed char>("signed char");
        print_traits<unsigned char>("unsigned char");
        print_traits<signed short>("signed short");
        print_traits<unsigned short>("unsigned short");
        print_traits<signed int>("signed int");
        print_traits<unsigned int>("unsigned int");
        print_traits<signed long>("signed long");
        print_traits<unsigned long>("unsigned long");
        print_traits<signed long long>("signed long long");
        print_traits<unsigned long long>("unsigned long long");

        return EXIT_SUCCESS;
    }


    sol_info("load/store/swap/compare_swap");
    {
        test_load_store_swap<bool>("bool");
        test_load_store_swap<wchar_t>("wchar_t");
        test_load_store_swap<signed char>("signed char");
        test_load_store_swap<unsigned char>("unsigned char");
        test_load_store_swap<signed short>("signed short");
        test_load_store_swap<unsigned short>("unsigned short");
        test_load_store_swap<signed int>("signed int");
        test_load_store_swap<unsigned int>("unsigned int");
        test_load_store_swap<signed long>("signed long");
        test_load_store_swap<unsigned long>("unsigned long");
        test_load_store_swap<signed long long>("signed long long");
        test_load_store_swap<unsigned long long>("unsigned long long");
    }


    sol_info("arithmetic");
    {
        test_arithmetic<wchar_t>("wchar_t");
        test_arithmetic<signed char>("signed char");
        test_arithmetic<unsigned char>("unsigned char");
        test_arithmetic<signed short>("signed short");
        test_arithmetic<unsigned short>("unsigned short");
        test_arithmetic<signed int>("signed int");
        test_arithmetic<unsigned int>("unsigned int");
        test_arithmetic<signed long>("signed long");
        test_arithmetic<unsigned long>("unsigned long");
        test_arithmetic<signed long long>("signed long long");
        test_arithmetic<unsigned long long>("unsigned long long");
    }


    sol_info("pointer");
    {
        test_pointer<wchar_t>("wchar_t");
        test_pointer<signed char>("signed char");
        test_pointer<unsigned char>("unsigned char");
        test_pointer<signed short>("signed short");
        test_pointer<unsigned short>("unsigned short");
        test_pointer<signed int>("signed int");
        test_pointer<unsigned int>("unsigned int");
        test_pointer<signed long>("signed long");
        test_pointer<unsigned long>("unsigned long");
        test_pointer<signed long long>("signed long long");
        test_pointer<unsigned long long>("unsigned long long");
    }

    return EXIT_SUCCESS;
}


}}  // namespace sol::test
