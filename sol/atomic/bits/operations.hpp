#if !defined(sol_atomic_bits_operations_hpp_included)
#define sol_atomic_bits_operations_hpp_included


// if next section does not define, use generic later
#if defined(SOL_ATOMIC_NO_GENERIC)
    #undef SOL_ATOMIC_NO_GENERIC
#endif


// include arch/compiler dependant implementation file
#include <sol/config.hpp>
#if defined(SOL_ARCH_X86)
    #if defined(SOL_COMPILER_GNU) || defined(SOL_COMPILER_INTEL)
        #define SOL_ATOMIC_NO_GENERIC
        #include <sol/atomic/bits/x86/operations-gcc.hpp>
    #endif
#elif defined(SOL_ARCH_X86_64)
    #if defined(SOL_COMPILER_GNU) || defined(SOL_COMPILER_INTEL)
        #define SOL_ATOMIC_NO_GENERIC
        #include <sol/atomic/bits/x86_64/operations-gcc.hpp>
    #endif
#endif


#if !defined(SOL_ATOMIC_NO_GENERIC)
    // not ported to current arch/compiler combo yet
    #define SOL_ATOMIC_no_bool
    #define SOL_ATOMIC_no_char
    #define SOL_ATOMIC_no_wchar
    #define SOL_ATOMIC_no_short
    #define SOL_ATOMIC_no_int
    #define SOL_ATOMIC_no_long
    #define SOL_ATOMIC_no_long_long
    #define SOL_ATOMIC_no_ptr
    #define SOL_ATOMIC_no_barrier
    #include <sol/atomic/bits/generic/operations.hpp>
#else
    #undef SOL_ATOMIC_NO_GENERIC
#endif


#endif  // sol_atomic_bits_operations_hpp_included
