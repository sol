#include <sol/config.hpp>


namespace sol {


char const * const sys_info::compiler_name = SOL_COMPILER_NAME;
char const * const sys_info::arch_name = SOL_ARCH_NAME;
char const * const sys_info::os_name = SOL_OS_NAME;


}   // namespace sol
