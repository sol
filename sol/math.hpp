#if !defined(sol_math_hpp_included)
#define sol_math_hpp_included
/// \defgroup sol_math sol::math
/// \ingroup sol
/// \{


/**
 * \file sol/math.hpp
 * \brief Math utilities
 *
 * Header(s):
 *  - \ref sol/math/abs.hpp
 *  - \ref sol/math/gcd.hpp
 *  - \ref sol/math/ratio.hpp
 *  - \ref sol/math/sign.hpp
 */


#include <sol/math/abs.hpp>
#include <sol/math/gcd.hpp>
#include <sol/math/ratio.hpp>
#include <sol/math/sign.hpp>


/// \}
#endif  // sol_math_hpp_included
