#include <sol/test_main.cpp>
#include <sol/math/abs.hpp>


namespace sol { namespace test {


namespace math = sol::math;


int
run (std::vector<std::string> const &args)
{
    sol_equal(math::abs<-1>::value, 1);
    sol_equal(math::abs<0>::value, 0);
    sol_equal(math::abs<1>::value, 1);

    return EXIT_SUCCESS;
}


}}  // namespace sol::test
