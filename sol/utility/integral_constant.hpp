#if !defined(sol_utility_integral_constant_hpp_included)
#define sol_utility_integral_constant_hpp_included
/// \addtogroup sol
/// \{


/**
 * \file sol/utility/integral_constant.hpp
 * \brief Helper classes to define integral constants
 * \author Sven Suursoho
 */


namespace sol {


/// Define integral constant of type \e T with value \e V
template <typename T, T V>
struct integral_constant
{
    /// Integral type
    typedef T value_type;

    /// Value
    static value_type const value = V;

    /// This type
    typedef integral_constant<T, V> type;
};


/// denote "no type"
struct null_type {};


/// typedef for true_type
typedef integral_constant<bool, true> true_type;


/// typedef for false_type
typedef integral_constant<bool, false> false_type;


}   // namespace sol


/// \}
#endif  // sol_utility_integral_constant_hpp_included
