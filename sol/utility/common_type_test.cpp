#include <sol/test_main.cpp>
#include <sol/utility/common_type.hpp>


namespace sol { namespace test {


// test that common type for type1 and type2 is expected_type
#define TEST_TYPES(type1,type2,expected_type) \
    do { \
        sol_equal(typeid(common_type<type1,type2>::type).name() \
            , typeid(expected_type).name() \
        ); \
        sol_equal(typeid(common_type<type2,type1>::type).name() \
            , typeid(expected_type).name() \
        ); \
    } while (0)


int
run (std::vector<std::string> const &args)
{
    using namespace sol;

    sol_info("bool");
    {
        typedef bool T;
        TEST_TYPES(T, T, T);
        TEST_TYPES(T, char, int);
        TEST_TYPES(T, unsigned char, int);
        TEST_TYPES(T, short, int);
        TEST_TYPES(T, unsigned short, int);
        TEST_TYPES(T, int, int);
        TEST_TYPES(T, unsigned int, unsigned int);
        TEST_TYPES(T, long, long);
        TEST_TYPES(T, unsigned long, unsigned long);
        TEST_TYPES(T, long long, long long);
        TEST_TYPES(T, unsigned long long, unsigned long long);
        TEST_TYPES(T, float, float);
        TEST_TYPES(T, double, double);
        TEST_TYPES(T, long double, long double);
    }

    sol_info("char");
    {
        typedef char T;
        TEST_TYPES(T, bool, int);
        TEST_TYPES(T, T, T);
        TEST_TYPES(T, unsigned char, int);
        TEST_TYPES(T, short, int);
        TEST_TYPES(T, unsigned short, int);
        TEST_TYPES(T, int, int);
        TEST_TYPES(T, unsigned int, unsigned int);
        TEST_TYPES(T, long, long);
        TEST_TYPES(T, unsigned long, unsigned long);
        TEST_TYPES(T, long long, long long);
        TEST_TYPES(T, unsigned long long, unsigned long long);
        TEST_TYPES(T, float, float);
        TEST_TYPES(T, double, double);
        TEST_TYPES(T, long double, long double);
    }

    sol_info("unsigned char");
    {
        typedef unsigned char T;
        TEST_TYPES(T, bool, int);
        TEST_TYPES(T, char, int);
        TEST_TYPES(T, T, T);
        TEST_TYPES(T, short, int);
        TEST_TYPES(T, unsigned short, int);
        TEST_TYPES(T, int, int);
        TEST_TYPES(T, unsigned int, unsigned int);
        TEST_TYPES(T, long, long);
        TEST_TYPES(T, unsigned long, unsigned long);
        TEST_TYPES(T, long long, long long);
        TEST_TYPES(T, unsigned long long, unsigned long long);
        TEST_TYPES(T, float, float);
        TEST_TYPES(T, double, double);
        TEST_TYPES(T, long double, long double);
    }

    sol_info("short");
    {
        typedef short T;
        TEST_TYPES(T, bool, int);
        TEST_TYPES(T, char, int);
        TEST_TYPES(T, unsigned char, int);
        TEST_TYPES(T, T, T);
        TEST_TYPES(T, unsigned short, int);
        TEST_TYPES(T, int, int);
        TEST_TYPES(T, unsigned int, unsigned int);
        TEST_TYPES(T, long, long);
        TEST_TYPES(T, unsigned long, unsigned long);
        TEST_TYPES(T, long long, long long);
        TEST_TYPES(T, unsigned long long, unsigned long long);
        TEST_TYPES(T, float, float);
        TEST_TYPES(T, double, double);
        TEST_TYPES(T, long double, long double);
    }

    sol_info("unsigned short");
    {
        typedef unsigned short T;
        TEST_TYPES(T, bool, int);
        TEST_TYPES(T, char, int);
        TEST_TYPES(T, unsigned char, int);
        TEST_TYPES(T, short, int);
        TEST_TYPES(T, T, T);
        TEST_TYPES(T, int, int);
        TEST_TYPES(T, unsigned int, unsigned int);
        TEST_TYPES(T, long, long);
        TEST_TYPES(T, unsigned long, unsigned long);
        TEST_TYPES(T, long long, long long);
        TEST_TYPES(T, unsigned long long, unsigned long long);
        TEST_TYPES(T, float, float);
        TEST_TYPES(T, double, double);
        TEST_TYPES(T, long double, long double);
    }

    sol_info("int");
    {
        typedef int T;
        TEST_TYPES(T, bool, int);
        TEST_TYPES(T, char, int);
        TEST_TYPES(T, unsigned char, int);
        TEST_TYPES(T, short, int);
        TEST_TYPES(T, unsigned short, int);
        TEST_TYPES(T, T, T);
        TEST_TYPES(T, unsigned int, unsigned int);
        TEST_TYPES(T, long, long);
        TEST_TYPES(T, unsigned long, unsigned long);
        TEST_TYPES(T, long long, long long);
        TEST_TYPES(T, unsigned long long, unsigned long long);
        TEST_TYPES(T, float, float);
        TEST_TYPES(T, double, double);
        TEST_TYPES(T, long double, long double);
    }

    sol_info("unsigned int");
    {
        typedef unsigned int T;
        TEST_TYPES(T, bool, unsigned int);
        TEST_TYPES(T, char, unsigned int);
        TEST_TYPES(T, unsigned char, unsigned int);
        TEST_TYPES(T, short, unsigned int);
        TEST_TYPES(T, unsigned short, unsigned int);
        TEST_TYPES(T, int, unsigned int);
        TEST_TYPES(T, T, T);
        TEST_TYPES(T, long, long);
        TEST_TYPES(T, unsigned long, unsigned long);
        TEST_TYPES(T, long long, long long);
        TEST_TYPES(T, unsigned long long, unsigned long long);
        TEST_TYPES(T, float, float);
        TEST_TYPES(T, double, double);
        TEST_TYPES(T, long double, long double);
    }

    sol_info("long");
    {
        typedef long T;
        TEST_TYPES(T, bool, long);
        TEST_TYPES(T, char, long);
        TEST_TYPES(T, unsigned char, long);
        TEST_TYPES(T, short, long);
        TEST_TYPES(T, unsigned short, long);
        TEST_TYPES(T, int, long);
        TEST_TYPES(T, unsigned int, long);
        TEST_TYPES(T, T, T);
        TEST_TYPES(T, unsigned long, unsigned long);
        TEST_TYPES(T, long long, long long);
        TEST_TYPES(T, unsigned long long, unsigned long long);
        TEST_TYPES(T, float, float);
        TEST_TYPES(T, double, double);
        TEST_TYPES(T, long double, long double);
    }

    sol_info("unsigned long");
    {
        typedef unsigned long T;
        TEST_TYPES(T, bool, unsigned long);
        TEST_TYPES(T, char, unsigned long);
        TEST_TYPES(T, unsigned char, unsigned long);
        TEST_TYPES(T, short, unsigned long);
        TEST_TYPES(T, unsigned short, unsigned long);
        TEST_TYPES(T, int, unsigned long);
        TEST_TYPES(T, unsigned int, unsigned long);
        TEST_TYPES(T, long, unsigned long);
        TEST_TYPES(T, T, T);
        TEST_TYPES(T, long long, unsigned long long);
        TEST_TYPES(T, unsigned long long, unsigned long long);
        TEST_TYPES(T, float, float);
        TEST_TYPES(T, double, double);
        TEST_TYPES(T, long double, long double);
    }

    sol_info("long long");
    {
        typedef long long T;
        TEST_TYPES(T, bool, long long);
        TEST_TYPES(T, char, long long);
        TEST_TYPES(T, unsigned char, long long);
        TEST_TYPES(T, short, long long);
        TEST_TYPES(T, unsigned short, long long);
        TEST_TYPES(T, int, long long);
        TEST_TYPES(T, unsigned int, long long);
        TEST_TYPES(T, long, long long);
        TEST_TYPES(T, unsigned long, unsigned long long);
        TEST_TYPES(T, T, T);
        TEST_TYPES(T, unsigned long long, unsigned long long);
        TEST_TYPES(T, float, float);
        TEST_TYPES(T, double, double);
        TEST_TYPES(T, long double, long double);
    }

    sol_info("unsigned long long");
    {
        typedef unsigned long long T;
        TEST_TYPES(T, bool, unsigned long long);
        TEST_TYPES(T, char, unsigned long long);
        TEST_TYPES(T, unsigned char, unsigned long long);
        TEST_TYPES(T, short, unsigned long long);
        TEST_TYPES(T, unsigned short, unsigned long long);
        TEST_TYPES(T, int, unsigned long long);
        TEST_TYPES(T, unsigned int, unsigned long long);
        TEST_TYPES(T, long, unsigned long long);
        TEST_TYPES(T, unsigned long, unsigned long long);
        TEST_TYPES(T, long long, unsigned long long);
        TEST_TYPES(T, T, T);
        TEST_TYPES(T, float, float);
        TEST_TYPES(T, double, double);
        TEST_TYPES(T, long double, long double);
    }

    sol_info("float");
    {
        typedef float T;
        TEST_TYPES(T, bool, float);
        TEST_TYPES(T, char, float);
        TEST_TYPES(T, unsigned char, float);
        TEST_TYPES(T, short, float);
        TEST_TYPES(T, unsigned short, float);
        TEST_TYPES(T, int, float);
        TEST_TYPES(T, unsigned int, float);
        TEST_TYPES(T, long, float);
        TEST_TYPES(T, unsigned long, float);
        TEST_TYPES(T, long long, float);
        TEST_TYPES(T, unsigned long long, float);
        TEST_TYPES(T, T, T);
        TEST_TYPES(T, double, double);
        TEST_TYPES(T, long double, long double);
    }

    sol_info("double");
    {
        typedef double T;
        TEST_TYPES(T, bool, double);
        TEST_TYPES(T, char, double);
        TEST_TYPES(T, unsigned char, double);
        TEST_TYPES(T, short, double);
        TEST_TYPES(T, unsigned short, double);
        TEST_TYPES(T, int, double);
        TEST_TYPES(T, unsigned int, double);
        TEST_TYPES(T, long, double);
        TEST_TYPES(T, unsigned long, double);
        TEST_TYPES(T, long long, double);
        TEST_TYPES(T, unsigned long long, double);
        TEST_TYPES(T, float, double);
        TEST_TYPES(T, T, T);
        TEST_TYPES(T, long double, long double);
    }

    sol_info("long double");
    {
        typedef long double T;
        TEST_TYPES(T, bool, long double);
        TEST_TYPES(T, char, long double);
        TEST_TYPES(T, unsigned char, long double);
        TEST_TYPES(T, short, long double);
        TEST_TYPES(T, unsigned short, long double);
        TEST_TYPES(T, int, long double);
        TEST_TYPES(T, unsigned int, long double);
        TEST_TYPES(T, long, long double);
        TEST_TYPES(T, unsigned long, long double);
        TEST_TYPES(T, long long, long double);
        TEST_TYPES(T, unsigned long long, long double);
        TEST_TYPES(T, float, long double);
        TEST_TYPES(T, double, long double);
        TEST_TYPES(T, T, T);
    }

    return EXIT_SUCCESS;
}


}}  // namespace sol::test
