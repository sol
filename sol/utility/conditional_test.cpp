#include <sol/test_main.cpp>
#include <sol/utility/conditional.hpp>
#include <sol/utility/integral_constant.hpp>


namespace sol { namespace test {


int
run (std::vector<std::string> const &args)
{
    using namespace sol;

    sol_assert(typeid(conditional<true, true_type, false_type>::type)
        == typeid(true_type)
    );

    sol_assert(typeid(conditional<false, true_type, false_type>::type)
        == typeid(false_type)
    );

    return EXIT_SUCCESS;
}


}}  // namespace sol::test
