#include <sol/test_main.cpp>
#include <sol/utility/integral_constant.hpp>


namespace sol { namespace test {


int
run (std::vector<std::string> const &args)
{
    using namespace sol;

    sol_equal(true_type::value, true);
    sol_assert(typeid(true_type::value_type) == typeid(bool));
    sol_assert(
        typeid(true_type::type) == typeid(integral_constant<bool, true>)
    );

    sol_equal(false_type::value, false);
    sol_assert(typeid(false_type::value_type) == typeid(bool));
    sol_assert(
        typeid(false_type::type) == typeid(integral_constant<bool, false>)
    );

    typedef integral_constant<int, 1> one_type;
    sol_equal(one_type::value, 1);
    sol_assert(typeid(one_type::value_type) == typeid(int));
    sol_assert(typeid(one_type::type) == typeid(integral_constant<int, 1>));
    sol_assert(typeid(one_type::type) != typeid(integral_constant<int, 2>));

    return EXIT_SUCCESS;
}


}}  // namespace sol::test
