sol_headers(
    ../math.hpp
    abs.hpp
    gcd.hpp
    ratio.hpp
    sign.hpp
)


sol_tests(
    abs_test.cpp
    gcd_test.cpp
    ratio_test.cpp
    sign_test.cpp
)
