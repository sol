#if !defined(sol_config_os_hpp_included)
#define sol_config_os_hpp_included
/// \addtogroup sol_config
/// \{


/**
 * \file sol/config/os.hpp
 * \brief OS-specific environment configuration
 * \author Sven Suursoho
 */


//
// detect
//

#if defined(SOL_OS_CONFIG)
    // already have user specified OS config

#elif defined(linux) || defined(__linux) || defined(__linux__)
    // linux
    #define SOL_OS_CONFIG "sol/config/os/linux.hpp"

#else
    #error "Unknown OS"
#endif


//
// include
//

#if defined(SOL_OS_CONFIG)
    #include SOL_OS_CONFIG
#endif


//
// check for required stuff
//

#if !defined(SOL_OS_NAME)
    #error "SOL_OS_NAME not specified"
#endif


/// \}
#endif  // sol_config_os_hpp_included
