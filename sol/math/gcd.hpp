#if !defined(sol_math_gcd_hpp_included)
#define sol_math_gcd_hpp_included
/// \addtogroup sol_math
/// \{


/**
 * \file sol/math/gcd.hpp
 * \brief Compile-time calculated greatest common divisor (GCD)
 *
 * \see http://everything2.org/e2node/C%252B%252B%253A%2520Checking%2520units%2520at%2520compile%2520time
 */


#include <sol/cstdint.hpp>
#include <sol/math/abs.hpp>
#include <sol/utility/integral_constant.hpp>


namespace sol { namespace math {


/// Calculate greatest common divisor of A and B at compile time
template <intmax_t A, intmax_t B>
struct gcd
    : gcd<B, (A%B)>
{};


/// \internal
template <intmax_t A>
struct gcd<A, 0>
    : integral_constant<intmax_t, abs<A>::value>
{};


}}  // namespace sol::math


/// \}
#endif  // sol_math_gcd_hpp_included
