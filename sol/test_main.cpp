//
// Test application main()
//
// Subtest should include this file and provide
// 'int run (std::vector<std::string> const &args);' with test cases
//


#include <cassert>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <typeinfo>
#include <vector>

#include <sys/times.h>
#include <unistd.h>


namespace sol { namespace test {


bool verbose(false);
bool print_location(false);
bool timing(false);
bool fatal_failure(false);
size_t failure_count(0), check_count(0);
char const *last_checkpoint(NULL);


int run (std::vector<std::string> const &args);


void
check_point (char const *location)
{
    ++check_count;
    last_checkpoint = location;
}


enum level_type { level_info, level_warn, level_error, level_count };


void
log_message (char const *msg, level_type level, char const *location)
{
    last_checkpoint = location;

    assert(level < level_count);

    char const level_chars[] = ">~!";
    char const level_char = level_chars[level];

    if (verbose || level == level_error) {
        std::cout << '\n' << level_char <<  '\t' << msg;
        if (location && (print_location || level == level_error)) {
            std::cout << " (" << location << ')';
        }
    }
    else {
        std::cout << level_char << std::flush;
    }
}


inline void
info (char const *msg, char const *location = NULL)
{
    log_message(msg, level_info, location);
}


inline void
warn (char const *msg, char const *location = NULL)
{
    std::ostringstream oss;
    oss << "warning: " << msg;
    log_message(oss.str().c_str(), level_warn, location);
}


void
error (char const *msg, char const *location = NULL)
{
    ++failure_count;

    std::ostringstream oss;
    oss << "error: " << msg;
    log_message(oss.str().c_str(), level_error, location);

    if (fatal_failure) {
        std::cout << std::endl;
        exit(EXIT_FAILURE);
    }
}


template <typename T1, typename T2>
void
equal_impl (T1 v1, T2 v2, char const *msg, char const *location)
{
    if (v1 == v2) {
        return;
    }

    std::ostringstream oss;
    oss << msg << " [" << v1 << " != " << v2 << ']';
    error(oss.str().c_str(), location);
}


template <typename T1, typename T2>
void
not_equal_impl (T1 v1, T2 v2, char const *msg, char const *location)
{
    if (v1 != v2) {
        return;
    }

    std::ostringstream oss;
    oss << msg << " [" << v1 << " == " << v2 << ']';
    error(oss.str().c_str(), location);
}


}} // sol::test


#define S(text) S_(text)
#define S_(text) #text

#define HERE __FILE__ ":" S(__LINE__)

#define sol_info(msg) sol::test::info(msg, HERE)
#define sol_warn(msg) sol::test::warn(msg, HERE)
#define sol_error(msg) sol::test::error(msg, HERE)

#define sol_assert(expr) \
    do { \
        sol::test::check_point(HERE); \
        if (!(expr)) { sol::test::error(S(expr), HERE); } \
    } while (false)

#define sol_equal(v1,v2) \
    do { \
        sol::test::check_point(HERE); \
        sol::test::equal_impl(v1, v2, S(v1 != v2), HERE); \
    } while (false)

#define sol_not_equal(v1,v2) \
    do { \
        sol::test::check_point(HERE); \
        sol::test::not_equal_impl(v1, v2, S(v1 == v2), HERE); \
    } while (false)

#define sol_check_throw_impl(statement,exception,exact) \
    do { \
        sol::test::check_point(HERE); \
        try { \
            statement; \
            sol::test::error("expected " S(exception), HERE); \
        } \
        catch (exception const &e) { \
            if (exact && typeid(exception) != typeid(e)) { \
                sol::test::error("expected " S(exception), HERE); \
            } \
        } \
        catch (...) { \
            sol::test::error("expected " S(exception), HERE); \
        } \
    } while (false)

#define sol_check_throw(statement,exception) \
    sol_check_throw_impl(statement,exception,false)
#define sol_check_exact_throw(statement,exception) \
    sol_check_throw_impl(statement,exception,true)

#define sol_check_no_throw(statement) \
    do { \
        sol::test::check_point(HERE); \
        try { \
            statement; \
        } \
        catch (...) { \
            sol::test::error("unexpected exception", HERE); \
        } \
    } while (false)


int
main (int argc, char *argv[])
{
    //
    // handle parameter(s)
    //

    std::vector<std::string> unhandled_args;
    for (int i = 1;  i != argc;  ++i) {
        std::string arg(argv[i]);

        if (arg == "-v" || arg == "--verbose") {
            sol::test::verbose = true;
        }
        else if (arg == "-l" || arg == "--location") {
            sol::test::print_location = true;
        }
        else if (arg == "-t" || arg == "--timing") {
            sol::test::timing = true;
        }
        else if (arg == "-f" || arg == "--fatal-failure") {
            sol::test::fatal_failure = true;
        }
        else {
            unhandled_args.push_back(arg);
        }
    }


    //
    // run
    //

    try {
        std::cout << argv[0] << ":\t" << std::flush;

        tms start;
        clock_t s(times(&start));

        int exit_value(sol::test::run(unhandled_args));

        tms end;
        clock_t e(times(&end));

        if (exit_value != EXIT_SUCCESS || sol::test::failure_count) {
            std::cout << "\n\tFAILED ("
                << sol::test::failure_count << '/' << sol::test::check_count
                << ')';
            exit_value = EXIT_FAILURE;
        }
        else {
            std::cout << (sol::test::verbose ? "\n\t" : " ")
                << "PASSED (" << sol::test::check_count << ')';
        }

        if (sol::test::timing) {
            double const tick(static_cast<double>(sysconf(_SC_CLK_TCK)));
            double const user((end.tms_utime - start.tms_utime)/tick*1000);
            double const sys((end.tms_stime - start.tms_stime)/tick*1000);
            double const real((e - s)/tick*1000);

            std::cout
                << " [real=" << real
                << "/user=" << user
                << "/sys=" << sys
                << "ms]"
            ;
        }

        std::cout << std::endl;
        return exit_value;
    }

    //
    // Handle unexpected stuff
    //

    catch (std::exception const &e) {
        std::cerr << "\n\t*** std::exception: " << e.what();
    }
    catch (char const *e) {
        std::cerr << "\n\t*** char const *: " << e;
    }
    catch (...) {
        std::cerr << "\n\t*** unknown error";
    }
    std::cerr << " (last checkpoint: "
        << (sol::test::last_checkpoint ? sol::test::last_checkpoint : "?")
        << ')' << std::endl;

    return EXIT_FAILURE;
}
