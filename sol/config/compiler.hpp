#if !defined(sol_config_compiler_hpp_included)
#define sol_config_compiler_hpp_included
/// \addtogroup sol_config
/// \{


/**
 * \file sol/config/compiler.hpp
 * \brief Compiler-specific configuration
 * \author Sven Suursoho
 */


//
// detect
//

#if defined(SOL_COMPILER_CONFIG)
    // already have user specified compiler config header

#elif defined(__GNUC__)
    // gcc
    #define SOL_COMPILER_CONFIG "sol/config/compiler/gcc.hpp"

#else
    #error "Unknown compiler"
#endif


//
// include
//

#if defined(SOL_COMPILER_CONFIG)
    #include SOL_COMPILER_CONFIG
#endif


//
// check for required stuff
//

#if !defined(SOL_COMPILER_NAME)
    #error "SOL_COMPILER_NAME not specified"
#endif

#if !defined(SOL_COMPILER_VERSION)
    #error "SOL_COMPILER_VERSION not specified"
#endif


/// \}
#endif  // sol_config_compiler_hpp_included
