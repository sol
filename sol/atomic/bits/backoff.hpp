#if !defined(sol_atomic_bits_backoff_hpp_included)
#define sol_atomic_bits_backoff_hpp_included


#include <ctime>


namespace sol { namespace atomic {


inline bool
spin_sleep::operator() (unsigned long current_iteration) const
{
    if (current_iteration < 13) {
        return false;
    }
    current_iteration -= 13;

    // precalculated sleep times
    static const timespec sleep_time[] = {
        // 13..27: sleep exponentially longer
        { 0, 1000 }
        , { 0, 2000 }
        , { 0, 4000 }
        , { 0, 8000 }
        , { 0, 16000 }
        , { 0, 32000 }
        , { 0, 64000 }
        , { 0, 128000 }
        , { 0, 256000 }
        , { 0, 512000 }
        , { 0, 1024000 }
        , { 0, 2048000 }
        , { 0, 4096000 }
        , { 0, 8192000 }
        , { 0, 16384000 }
        , { 0, 32768000 }

        // 28: but no more than this
        , { 0, 65536000 }
    };
    static const size_t sleep_count(
        sizeof(sleep_time)/sizeof(sleep_time[0]) - 1
    );

    // do sleep
    if (current_iteration > sleep_count) {
        current_iteration = sleep_count;
    }
    nanosleep(&sleep_time[current_iteration], 0);

    return false;
}


}}  // namespace sol::atomic


#endif  // sol_atomic_bits_backoff_hpp_included
