#if !defined(sol_thread_hpp_included)
#define sol_thread_hpp_included
/// \defgroup sol_thread sol::thread
/// \ingroup sol
/// \{


/**
 * \file sol/thread.hpp
 * \brief Threading API
 *
 * Header(s):
 *  - sol/thread/lock_pool.hpp
 */


#include <sol/thread/lock_pool.hpp>


/// \}
#endif  // sol_thread_hpp_included
