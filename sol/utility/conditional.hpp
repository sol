#if !defined(sol_utility_conditional_hpp_included)
#define sol_utility_conditional_hpp_included
/// \addtogroup sol
/// \{


/**
 * \file sol/utility/conditional.hpp
 * \brief Conditional expression for types
 * \author Sven Suursoho
 */


namespace sol {


/// If \e Cond, then typedef TrueType as type, else FalseType
template <bool Cond, typename TrueType, typename FalseType>
struct conditional
{
    typedef TrueType type;
};


template <typename TrueType, typename FalseType>
struct conditional<false, TrueType, FalseType>
{
    typedef FalseType type;
};


}   // namespace sol


/// \}
#endif  // sol_utility_conditional_hpp_included
