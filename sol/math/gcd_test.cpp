#include <sol/test_main.cpp>
#include <sol/math/gcd.hpp>
#include <limits>


namespace sol { namespace test {


namespace math = sol::math;


intmax_t
gcd (intmax_t a, intmax_t b)
{
    intmax_t tmp;
    while (b != 0) {
        tmp = a % b;
        a = b;
        b = tmp;
    }
    return (a < 0) ? -a : a;
}


int
run (std::vector<std::string> const &args)
{
    sol_equal((math::gcd<   0,    0 >::value), gcd(   0,    0 ));
    sol_equal((math::gcd<   1,    0 >::value), gcd(   1,    0 ));
    sol_equal((math::gcd<  -1,    0 >::value), gcd(  -1,    0 ));
    sol_equal((math::gcd<   0,    1 >::value), gcd(   0,    1 ));
    sol_equal((math::gcd<   0,   -1 >::value), gcd(   0,   -1 ));
    sol_equal((math::gcd<   7,    1 >::value), gcd(   7,    1 ));
    sol_equal((math::gcd<   7,   -1 >::value), gcd(   7,   -1 ));
    sol_equal((math::gcd< -23,   15 >::value), gcd( -23,   15 ));
    sol_equal((math::gcd< 120,   84 >::value), gcd( 120,   84 ));
    sol_equal((math::gcd<  84, -120 >::value), gcd(  84, -120 ));

    return EXIT_SUCCESS;
}


}}  // namespace sol::test
