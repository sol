#if !defined(sol_atomic_operations_hpp_included)
#define sol_atomic_operations_hpp_included
/// \addtogroup sol_atomic
/// \{


/**
 * \file sol/atomic/operations.hpp
 * \brief Low-level architecture dependant operations
 * \author Sven Suursoho
 */


namespace sol { namespace atomic {


/// \defgroup atomic_memory_ordering Memory ordering
/// \{

/// The operation does not order memory
enum relaxed_type { relaxed };

/// Performs an acquire operation on the affected memory location
enum acquire_type { acquire };

/// Performs a release operation on the affected memory location
enum release_type { release };

/// The operation has both acquire and release semantics
enum acq_rel_type { acq_rel };

/// The operation has both acquire and release semantics, and in addition, has
/// sequentially-consistent operation ordering
enum seq_cst_type { seq_cst };

/// \}


// load {{{

/// Provide info for atomic::load atomicity properties
template <typename T>
struct load_op
{
    /// If \c true, atomic::load is lock free
    static bool const lock_free;

    /// If \c true, atomic::load is wait free
    static bool const wait_free;
};

/// Return value at address \a a (unordered)
template <typename T>
T
load (T volatile const *a, relaxed_type);

/// Return value at address \a a (unordered)
template <typename T>
T *
load (T * volatile const *a, relaxed_type);

/// Return value at address \a a (acquire)
template <typename T>
T
load (T volatile const *a, acquire_type);

/// Return value at address \a a (acquire)
template <typename T>
T *
load (T * volatile const *a, acquire_type);

/// Return value at address \a a (ordered)
template <typename T>
T
load (T volatile const *a, seq_cst_type);

/// Return value at address \a a (ordered)
template <typename T>
T *
load (T * volatile const *a, seq_cst_type);

// }}}


// store {{{

/// Provide info for atomic::store atomicity properties
template <typename T>
struct store_op
{
    /// If \c true, atomic::store is lock free
    static bool const lock_free;

    /// If \c true, atomic::store is wait free
    static bool const wait_free;
};

/// Store \a v into \a a (unordered). Return \a v
template <typename T>
T
store (T volatile *a, T v, relaxed_type);

/// Store \a v into \a a (unordered). Return \a v
template <typename T>
T *
store (T * volatile *a, T const *v, relaxed_type);

/// Store \a v into \a a (release). Return \a v
template <typename T>
T
store (T volatile *a, T v, release_type);

/// Store \a v into \a a (release). Return \a v
template <typename T>
T *
store (T * volatile *a, T const *v, release_type);

/// Store \a v into \a a (ordered). Return \a v
template <typename T>
T
store (T volatile *a, T v, seq_cst_type);

/// Store \a v into \a a (ordered). Return \a v
template <typename T>
T *
store (T * volatile *a, T const *v, seq_cst_type);

// }}}


// swap {{{

/// Provide info for atomic::swap atomicity properties
template <typename T>
struct swap_op
{
    /// If \c true, atomic::swap is lock free
    static bool const lock_free;

    /// If \c true, atomic::swap is wait free
    static bool const wait_free;
};

/// Store \a v into \a a (unordered). Return old value of \a a
template <typename T>
T
swap (T volatile *a, T v, relaxed_type);

/// Store \a v into \a a (unordered). Return old value of \a a
template <typename T>
T *
swap (T * volatile *a, T const *v, relaxed_type);

/// Store \a v into \a a (acquire). Return old value of \a a
template <typename T>
T
swap (T volatile *a, T v, acquire_type);

/// Store \a v into \a a (acquire). Return old value of \a a
template <typename T>
T *
swap (T * volatile *a, T const *v, acquire_type);

/// Store \a v into \a a (release). Return old value of \a a
template <typename T>
T
swap (T volatile *a, T v, release_type);

/// Store \a v into \a a (release). Return old value of \a a
template <typename T>
T *
swap (T * volatile *a, T const *v, release_type);

/// Store \a v into \a a (ordered). Return old value of \a a
template <typename T>
T
swap (T volatile *a, T v, acq_rel_type);

/// Store \a v into \a a (ordered). Return old value of \a a
template <typename T>
T *
swap (T * volatile *a, T const *v, acq_rel_type);

/// Store \a v into \a a (sequentially-consistent). Return old value of \a a
template <typename T>
T
swap (T volatile *a, T v, seq_cst_type);

/// Store \a v into \a a (sequentially-consistent). Return old value of \a a
template <typename T>
T *
swap (T * volatile *a, T const *v, seq_cst_type);

// }}}


// compare_swap {{{

/// Provide info for atomic::compare_swap atomicity properties
template <typename T>
struct compare_swap_op
{
    /// If \c true, atomic::compare_swap is lock free
    static bool const lock_free;

    /// If \c true, atomic::compare_swap is wait free
    static bool const wait_free;
};

/**
 * Atomically check if \a *a equals \a e. If yes, store \a v into \a *a and
 * return true. Otherwise return false.
 */
template <typename T>
bool
compare_swap (T volatile *a, T e, T v, relaxed_type);

/**
 * Atomically check if \a *a equals \a e. If yes, store \a v into \a *a and
 * return true. Otherwise return false.
 */
template <typename T>
bool
compare_swap (T * volatile *a, T const *e, T const *v, relaxed_type);

/**
 * Atomically check if \a *a equals \a e. If yes, store \a v into \a *a and
 * return true. Otherwise return false.
 */
template <typename T>
bool
compare_swap (T volatile *a, T e, T v, acquire_type);

/**
 * Atomically check if \a *a equals \a e. If yes, store \a v into \a *a and
 * return true. Otherwise return false.
 */
template <typename T>
bool
compare_swap (T * volatile *a, T const *e, T const *v, acquire_type);

/**
 * Atomically check if \a *a equals \a e. If yes, store \a v into \a *a and
 * return true. Otherwise return false.
 */
template <typename T>
bool
compare_swap (T volatile *a, T e, T v, release_type);

/**
 * Atomically check if \a *a equals \a e. If yes, store \a v into \a *a and
 * return true. Otherwise return false.
 */
template <typename T>
bool
compare_swap (T * volatile *a, T const *e, T const *v, release_type);

/**
 * Atomically check if \a *a equals \a e. If yes, store \a v into \a *a and
 * return true. Otherwise return false.
 */
template <typename T>
bool
compare_swap (T volatile *a, T e, T v, acq_rel_type);

/**
 * Atomically check if \a *a equals \a e. If yes, store \a v into \a *a and
 * return true. Otherwise return false.
 */
template <typename T>
bool
compare_swap (T * volatile *a, T const *e, T const *v, acq_rel_type);

/**
 * Atomically check if \a *a equals \a e. If yes, store \a v into \a *a and
 * return true. Otherwise return false.
 */
template <typename T>
bool
compare_swap (T volatile *a, T e, T v, seq_cst_type);

/**
 * Atomically check if \a *a equals \a e. If yes, store \a v into \a *a and
 * return true. Otherwise return false.
 */
template <typename T>
bool
compare_swap (T * volatile *a, T const *e, T const *v, seq_cst_type);

// }}}


// fetch_add {{{

/// Provide info for atomic::fetch_add atomicity properties
template <typename T>
struct fetch_add_op
{
    /// If \c true, atomic::fetch_add is lock free
    static bool const lock_free;

    /// If \c true, atomic::fetch_add is wait free
    static bool const wait_free;
};

/// Add \a v to \a a and return old value of \a a (unordered)
template <typename T>
T
fetch_add (T volatile *a, T v, relaxed_type);

/// Add \a v to \a a and return old value of \a a (unordered)
template <typename T>
T *
fetch_add (T * volatile *a, ptrdiff_t v, relaxed_type);

/// Add \a v to \a a and return old value of \a a (acquire)
template <typename T>
T
fetch_add (T volatile *a, T v, acquire_type);

/// Add \a v to \a a and return old value of \a a (acquire)
template <typename T>
T *
fetch_add (T * volatile *a, ptrdiff_t v, acquire_type);

/// Add \a v to \a a and return old value of \a a (release)
template <typename T>
T
fetch_add (T volatile *a, T v, release_type);

/// Add \a v to \a a and return old value of \a a (release)
template <typename T>
T *
fetch_add (T * volatile *a, ptrdiff_t v, release_type);

/// Add \a v to \a a and return old value of \a a (ordered)
template <typename T>
T
fetch_add (T volatile *a, T v, acq_rel_type);

/// Add \a v to \a a and return old value of \a a (ordered)
template <typename T>
T *
fetch_add (T * volatile *a, ptrdiff_t v, acq_rel_type);

/// Add \a v to \a a and return old value of \a a (sequentially-consistent)
template <typename T>
T
fetch_add (T volatile *a, T v, seq_cst_type);

/// Add \a v to \a a and return old value of \a a (sequentially-consistent)
template <typename T>
T *
fetch_add (T * volatile *a, ptrdiff_t v, seq_cst_type);

// }}}


// fetch_sub {{{

/// Provide info for atomic::fetch_sub atomicity properties
template <typename T>
struct fetch_sub_op
{
    /// If \c true, atomic::fetch_sub is lock free
    static bool const lock_free;

    /// If \c true, atomic::fetch_sub is wait free
    static bool const wait_free;
};

/// Subtract \a v from \a a and return old value of \a a (unordered)
template <typename T>
T
fetch_sub (T volatile *a, T v, relaxed_type);

/// Subtract \a v from \a a and return old value of \a a (unordered)
template <typename T>
T *
fetch_sub (T * volatile *a, ptrdiff_t v, relaxed_type);

/// Subtract \a v from \a a and return old value of \a a (acquire)
template <typename T>
T
fetch_sub (T volatile *a, T v, acquire_type);

/// Subtract \a v from \a a and return old value of \a a (acquire)
template <typename T>
T *
fetch_sub (T * volatile *a, ptrdiff_t v, acquire_type);

/// Subtract \a v from \a a and return old value of \a a (release)
template <typename T>
T
fetch_sub (T volatile *a, T v, release_type);

/// Subtract \a v from \a a and return old value of \a a (release)
template <typename T>
T *
fetch_sub (T * volatile *a, ptrdiff_t v, release_type);

/// Subtract \a v from \a a and return old value of \a a (ordered)
template <typename T>
T
fetch_sub (T volatile *a, T v, acq_rel_type);

/// Subtract \a v from \a a and return old value of \a a (ordered)
template <typename T>
T *
fetch_sub (T * volatile *a, ptrdiff_t v, acq_rel_type);

/// Subtract \a v from \a a and return old value of \a a
/// (sequentially-consistent)
template <typename T>
T
fetch_sub (T volatile *a, T v, seq_cst_type);

/// Subtract \a v from \a a and return old value of \a a
/// (sequentially-consistent)
template <typename T>
T *
fetch_sub (T * volatile *a, ptrdiff_t v, seq_cst_type);

// }}}


// fetch_and {{{

/// Provide info for atomic::fetch_and atomicity properties
template <typename T>
struct fetch_and_op
{
    /// If \c true, atomic::fetch_and is lock free
    static bool const lock_free;

    /// If \c true, atomic::fetch_and is wait free
    static bool const wait_free;
};

/// Store into \a a result of \a a AND \a v. Return old value of \a a
/// (unordered)
template <typename T>
T
fetch_and (T volatile *a, T v, relaxed_type);

/// Store into \a a result of \a a AND \a v. Return old value of \a a
/// (acquire)
template <typename T>
T
fetch_and (T volatile *a, T v, acquire_type);

/// Store into \a a result of \a a AND \a v. Return old value of \a a
/// (release)
template <typename T>
T
fetch_and (T volatile *a, T v, release_type);

/// Store into \a a result of \a a AND \a v. Return old value of \a a
/// (ordered)
template <typename T>
T
fetch_and (T volatile *a, T v, acq_rel_type);

/// Store into \a a result of \a a AND \a v. Return old value of \a a
/// (sequentially-consistent)
template <typename T>
T
fetch_and (T volatile *a, T v, seq_cst_type);

// }}}


// fetch_or {{{

/// Provide info for atomic::fetch_or atomicity properties
template <typename T>
struct fetch_or_op
{
    /// If \c true, atomic::fetch_or is lock free
    static bool const lock_free;

    /// If \c true, atomic::fetch_or is wait free
    static bool const wait_free;
};

/// Store into \a a result of \a a OR \a v. Return old value of \a a
/// (unordered)
template <typename T>
T
fetch_or (T volatile *a, T v, relaxed_type);

/// Store into \a a result of \a a OR \a v. Return old value of \a a
/// (acquire)
template <typename T>
T
fetch_or (T volatile *a, T v, acquire_type);

/// Store into \a a result of \a a OR \a v. Return old value of \a a
/// (release)
template <typename T>
T
fetch_or (T volatile *a, T v, release_type);

/// Store into \a a result of \a a OR \a v. Return old value of \a a
/// (ordered)
template <typename T>
T
fetch_or (T volatile *a, T v, acq_rel_type);

/// Store into \a a result of \a a OR \a v. Return old value of \a a
/// (sequentially-consistent)
template <typename T>
T
fetch_or (T volatile *a, T v, seq_cst_type);

// }}}


// fetch_xor {{{

/// Provide info for atomic::fetch_xor atomicity properties
template <typename T>
struct fetch_xor_op
{
    /// If \c true, atomic::fetch_xor is lock free
    static bool const lock_free;

    /// If \c true, atomic::fetch_xor is wait free
    static bool const wait_free;
};

/// Store into \a a result of \a a XOR \a v. Return old value of \a a
/// (unordered)
template <typename T>
T
fetch_xor (T volatile *a, T v, relaxed_type);

/// Store into \a a result of \a a XOR \a v. Return old value of \a a
/// (acquire)
template <typename T>
T
fetch_xor (T volatile *a, T v, acquire_type);

/// Store into \a a result of \a a XOR \a v. Return old value of \a a
/// (release)
template <typename T>
T
fetch_xor (T volatile *a, T v, release_type);

/// Store into \a a result of \a a XOR \a v. Return old value of \a a
/// (ordered)
template <typename T>
T
fetch_xor (T volatile *a, T v, acq_rel_type);

/// Store into \a a result of \a a XOR \a v. Return old value of \a a
/// (sequentially-consistent)
template <typename T>
T
fetch_xor (T volatile *a, T v, seq_cst_type);

// }}}


}}  // namespace sol::atomic


#include <sol/atomic/bits/operations.hpp>


/// \}
#endif  // sol_atomic_operations_hpp_included
