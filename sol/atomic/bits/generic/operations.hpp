#if !defined(sol_atomic_bits_generic_operations_hpp_included)
#define sol_atomic_bits_generic_operations_hpp_included


#include <sol/thread/lock_pool.hpp>


namespace sol { namespace atomic {


namespace bits {

typedef lock_type::scoped_lock guard_type;
typedef thread::lock_pool<lock_type> lock_pool_type;

inline lock_type &
op_lock (const void *address)
{
    static lock_pool_type locks(32);
    return locks(address);
}

}   // namespace bits


}}  // namespace sol::atomic


#endif  // sol_atomic_bits_generic_operations_hpp_included
