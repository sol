#include <sol/test_main.cpp>
#include <sol/math/ratio.hpp>


namespace sol { namespace test {


namespace math = sol::math;


int
run (std::vector<std::string> const &args)
{
    sol_info("instantiate");
    {
        math::ratio<1,4> r0;
        sol_equal(r0.num, 1);
        sol_equal(r0.den, 4);

        math::ratio<2,8> r1;
        sol_equal(r1.num, r0.num);
        sol_equal(r1.den, r0.den);

        math::ratio<2,-8> r2;
        sol_equal(r2.num, -r0.num);
        sol_equal(r2.den, r0.den);

        math::ratio<-2,8> r3;
        sol_equal(r3.num, -r0.num);
        sol_equal(r3.den, r0.den);
    }


    sol_info("add");
    {
        math::ratio_add<
            math::ratio<5,7>
            , math::ratio<2,3>
        >::type r;

        sol_equal(r.num, 29);
        sol_equal(r.den, 21);
    }


    sol_info("subtract");
    {
        math::ratio_subtract<
            math::ratio<5,7>
            , math::ratio<2,3>
        >::type r;

        sol_equal(r.num, 1);
        sol_equal(r.den, 21);
    }


    sol_info("multiply");
    {
        math::ratio_multiply<
            math::ratio<5,7>
            , math::ratio<2,3>
        >::type r;

        sol_equal(r.num, 10);
        sol_equal(r.den, 21);
    }


    sol_info("multiply");
    {
        math::ratio_divide<
            math::ratio<5,7>
            , math::ratio<2,3>
        >::type r;

        sol_equal(r.num, 15);
        sol_equal(r.den, 14);
    }


    sol_info("equal");
    {
        sol_equal(true
            , (math::ratio_equal<
                math::ratio<1,2>
                , math::ratio<1,2>
              >::value)
        );
        sol_equal(false
            , (math::ratio_equal<
                math::ratio<1,3>
                , math::ratio<1,2>
              >::value)
        );
    }


    sol_info("not_equal");
    {
        sol_equal(false
            , (math::ratio_not_equal<
                math::ratio<1,2>
                , math::ratio<1,2>
              >::value)
        );
        sol_equal(true
            , (math::ratio_not_equal<
                math::ratio<1,3>
                , math::ratio<1,2>
              >::value)
        );
    }


    sol_info("less");
    {
        sol_equal(true
            , (math::ratio_less<
                math::ratio<1,3>
                , math::ratio<1,2>
              >::value)
        );
        sol_equal(false
            , (math::ratio_less<
                math::ratio<1,2>
                , math::ratio<1,3>
              >::value)
        );
    }


    sol_info("less_equal");
    {
        sol_equal(true
            , (math::ratio_less_equal<
                math::ratio<1,3>
                , math::ratio<1,2>
              >::value)
        );
        sol_equal(true
            , (math::ratio_less_equal<
                math::ratio<1,3>
                , math::ratio<1,3>
              >::value)
        );
        sol_equal(false
            , (math::ratio_less_equal<
                math::ratio<1,2>
                , math::ratio<1,3>
              >::value)
        );
    }


    sol_info("greater");
    {
        sol_equal(false
            , (math::ratio_greater<
                math::ratio<1,3>
                , math::ratio<1,2>
              >::value)
        );
        sol_equal(true
            , (math::ratio_greater<
                math::ratio<1,2>
                , math::ratio<1,3>
              >::value)
        );
    }


    sol_info("greater_equal");
    {
        sol_equal(true
            , (math::ratio_greater_equal<
                math::ratio<1,2>
                , math::ratio<1,3>
              >::value)
        );
        sol_equal(true
            , (math::ratio_greater_equal<
                math::ratio<1,3>
                , math::ratio<1,3>
              >::value)
        );
        sol_equal(false
            , (math::ratio_greater_equal<
                math::ratio<1,3>
                , math::ratio<1,2>
              >::value)
        );
    }


    return EXIT_SUCCESS;
}


}}  // namespace sol::test
