#include <sol/test_main.cpp>
#include <sol/date_time/utc_time.hpp>
#include <sol/date_time/hiresolution_clock.hpp>
#include <ctime>


namespace sol { namespace test {


namespace date_time = sol::date_time;


int
run (std::vector<std::string> const &args)
{
  date_time::utc_time now(date_time::hiresolution_clock::universal_time());

  sol_info("hiresolution clock");
  {
      date_time::utc_time t1(now + date_time::seconds(1));
      sol_equal(now.seconds_since_epoch() + 1, t1.seconds_since_epoch());

      std::time_t t2(std::time(0));
      sol_equal((t2 - now.seconds_since_epoch()) <= 1, true);
  }


  sol_info("comparison");
  {
      date_time::utc_time t2;

      sol_equal((now == t2), false);
      sol_equal((now != t2), true);
      sol_equal((now > t2), true);
      sol_equal((now >= t2), true);
      sol_equal((now < t2), false);
      sol_equal((now <= t2), false);

      t2 = now;
      sol_equal((now == t2), true);
      sol_equal((now != t2), false);
      sol_equal((now > t2), false);
      sol_equal((now >= t2), true);
      sol_equal((now < t2), false);
      sol_equal((now <= t2), true);

      t2 = now + date_time::seconds(1);
      sol_equal((now == t2), false);
      sol_equal((now != t2), true);
      sol_equal((now > t2), false);
      sol_equal((now >= t2), false);
      sol_equal((now < t2), true);
      sol_equal((now <= t2), true);
  }


  return EXIT_SUCCESS;
}


}}   // namespace sol::test
