#if !defined(sol_atomic_hpp_included)
#define sol_atomic_hpp_included
/// \defgroup sol_atomic sol::atomic
/// \ingroup sol
/// \{


/**
 * \file sol/atomic.hpp
 * \brief Machine dependant atomic operations
 *
 * Header(s):
 *  - sol/atomic/backoff.hpp
 *  - sol/atomic/flag.hpp
 *  - sol/atomic/operations.hpp
 */


#include <sol/atomic/backoff.hpp>
#include <sol/atomic/flag.hpp>
#include <sol/atomic/operations.hpp>


/// \}
#endif  // sol_atomic_hpp_included
