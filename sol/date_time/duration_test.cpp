#include <sol/test_main.cpp>
#include <sol/date_time/duration.hpp>


namespace sol { namespace test {


namespace date_time = sol::date_time;


template <typename duration_type>
void
check_arithmetic ()
{
  typename duration_type::tick_type ticks(2);
  duration_type d1(ticks), d2(d1);

  sol_equal(ticks, d1.get_count());
  sol_equal(ticks, d2.get_count());
  sol_equal(d1.get_count(), d2.get_count());

  sol_equal(ticks + 2, (d1 + 2).get_count());
  sol_equal(ticks + 2, (d2 + 2).get_count());
  sol_equal((d1 + 2).get_count(), (d2 + 2).get_count());

  ticks += 2;  d1 += 2;  d2 += 2;
  sol_equal(ticks, d1.get_count());
  sol_equal(ticks, d2.get_count());
  sol_equal(d1.get_count(), d2.get_count());

  sol_equal(ticks - 2, (d1 - 2).get_count());
  sol_equal(ticks - 2, (d2 - 2).get_count());
  sol_equal((d1 - 2).get_count(), (d2 - 2).get_count());

  ticks -= 2;  d1 -= 2;  d2 -= 2;
  sol_equal(ticks, d1.get_count());
  sol_equal(ticks, d2.get_count());
  sol_equal(d1.get_count(), d2.get_count());

  sol_equal(ticks*2, (d1*2).get_count());
  sol_equal(ticks*2, (d2*2).get_count());
  sol_equal((d1*2).get_count(), (d2*2).get_count());

  ticks *= 2;  d1 *= 2;  d2 *= 2;
  sol_equal(ticks, d1.get_count());
  sol_equal(ticks, d2.get_count());
  sol_equal(d1.get_count(), d2.get_count());

  sol_equal(ticks/2, (d1/2).get_count());
  sol_equal(ticks/2, (d2/2).get_count());
  sol_equal((d1/2).get_count(), (d2/2).get_count());

  ticks /= 2;  d1 /= 2;  d2 /= 2;
  sol_equal(ticks, d1.get_count());
  sol_equal(ticks, d2.get_count());
  sol_equal(d1.get_count(), d2.get_count());
}


template <typename duration_type>
void
check_comparison ()
{
  {
      duration_type d(1);
      date_time::nanoseconds ns(0);
      sol_equal((ns == d), false);
      sol_equal((ns != d), true);
      sol_equal((ns < d), true);
      sol_equal((ns <= d), true);
      sol_equal((ns > d), false);
      sol_equal((ns >= d), false);
  }

  {
      duration_type d(0);
      date_time::nanoseconds ns(1);
      sol_equal((ns == d), false);
      sol_equal((ns != d), true);
      sol_equal((ns < d), false);
      sol_equal((ns <= d), false);
      sol_equal((ns > d), true);
      sol_equal((ns >= d), true);
  }
}


int
run (std::vector<std::string> const &args)
{
  sol_info("arithmetic");
  {
      check_arithmetic<date_time::nanoseconds>();
      check_arithmetic<date_time::microseconds>();
      check_arithmetic<date_time::milliseconds>();
      check_arithmetic<date_time::seconds>();
      check_arithmetic<date_time::minutes>();
      check_arithmetic<date_time::hours>();
  }


  sol_info("comparison");
  {
      check_comparison<date_time::nanoseconds>();
      check_comparison<date_time::microseconds>();
      check_comparison<date_time::milliseconds>();
      check_comparison<date_time::seconds>();
      check_comparison<date_time::minutes>();
      check_comparison<date_time::hours>();
  }


  return EXIT_SUCCESS;
}


}}   // namespace sol::test
