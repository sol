#if !defined(sol_math_abs_hpp_included)
#define sol_math_abs_hpp_included
/// \addtogroup sol_math
/// \{


/**
 * \file sol/math/abs.hpp
 * \brief Calculate absolute value during compile-time
 * \author Sven Suursoho
 */


#include <sol/cstdint.hpp>
#include <sol/utility/integral_constant.hpp>


namespace sol { namespace math {


/// Calculate absolute value of V during compile-time
template <intmax_t V>
struct abs
    : integral_constant<intmax_t, (V < 0) ? -V : V>
{};


}}  // namespace sol::math


/// \}
#endif  // sol_math_abs_hpp_included
