#if !defined(sol_noncopyable_hpp_included)
#define sol_noncopyable_hpp_included
/// \addtogroup sol
/// \{


/**
 * \file sol/noncopyable.hpp
 * \brief Convenience base class providing copy protection for inherited classes
 * \author Sven Suursoho
 */


namespace sol {


/// Classes inherited from sol::noncopyable can't be copied
class noncopyable
{
protected:

    noncopyable () {}
    ~noncopyable () {}


private:

    noncopyable (noncopyable const &);
    noncopyable &operator= (noncopyable const &);
};


}   // namespace sol


/// \}
#endif  // sol_noncopyable_hpp_included
