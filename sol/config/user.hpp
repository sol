#if !defined(sol_config_user_hpp_included)
#define sol_config_user_hpp_included
/// \addtogroup sol_config
/// \{


/**
 * \file sol/config/compiler.hpp
 * \brief User-defined project configuration
 * \author Sven Suursoho
 */




/// \}
#endif  // sol_config_user_hpp_included
