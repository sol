#if !defined(sol_date_time_hpp_included)
#define sol_date_time_hpp_included
/// \defgroup sol_date_time sol::date_time
/// \ingroup sol
/// \{


/**
 * \file sol/date_time.hpp
 * \brief Minimal date/time operations to support threading
 *
 * \see http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2328.html
 *
 * Headers(s):
 *  - \ref sol/date_time/duration.hpp
 *  - \ref sol/date_time/hiresolution_clock.hpp
 *  - \ref sol/date_time/utc_time.hpp
 */


#include <sol/date_time/duration.hpp>
#include <sol/date_time/hiresolution_clock.hpp>
#include <sol/date_time/utc_time.hpp>


/// \}
#endif  // sol_date_time_hpp_included
