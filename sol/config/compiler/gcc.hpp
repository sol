#if !defined(sol_config_compiler_gcc_hpp_included)
#define sol_config_compiler_gcc_hpp_included
/// \addtogroup sol_config
/// \{


/**
 * \file sol/config/compiler/gcc.hpp
 * \brief gcc-specific compiler configuration
 * \author Sven Suursoho
 */


#include <sol/version.hpp>


//
// set compiler name and version
//

#define SOL_COMPILER_GNU 1

#define SOL_COMPILER_NAME "g++"

#define SOL_COMPILER_VERSION \
    SOL_MAKE_VERSION(__GNUC__,__GNUC_MINOR__,__GNUC_PATCHLEVEL__)

//
// long long
//

#define SOL_HAS_LONG_LONG


//
// C++0x features
//

#if SOL_COMPILER_VERSION >= SOL_MAKE_VERSION(4,3,0)
    #if defined(__GXX_EXPERIMENTAL_CXX0X__)
        #define SOL_HAS_STATIC_ASSERT
        #define SOL_HAS_VARIADIC_TEMPLATE
        #define SOL_HAS_DECLTYPE
    #endif
#endif


/// \}
#endif  // sol_config_compiler_gcc_hpp_included
