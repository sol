#if !defined(sol_preprocessor_cpp_included)
#define sol_preprocessor_cpp_included
/// \defgroup sol_preprocessor
/// \ingroup sol
/// \{


/**
 * \file sol/preprocessor.hpp
 * \brief Preprocessor helper stuff
 * \author Sven Suursoho
 */


#include <sol/preprocessor/here.hpp>
#include <sol/preprocessor/join.hpp>
#include <sol/preprocessor/text.hpp>


/// \}
#endif  // sol_preprocessor_cpp_included
