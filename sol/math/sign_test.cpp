#include <sol/test_main.cpp>
#include <sol/math/sign.hpp>


namespace sol { namespace test {


namespace math = sol::math;


int
run (std::vector<std::string> const &args)
{
    sol_equal(math::sign<-2>::value, -1);
    sol_equal(math::sign<0>::value, 1);
    sol_equal(math::sign<2>::value, 1);

    return EXIT_SUCCESS;
}


}}  // namespace sol::test
